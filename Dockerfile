FROM php:7.4-apache

COPY public_html /var/www/html/
COPY toast-php.ini /usr/local/etc/php/conf.d/toast-php.ini
COPY createImg.sh /createImg.sh

RUN chmod 777 /var/www/html/temp
RUN chmod 777 /createImg.sh
RUN chmod 777 /tmp

RUN a2enmod rewrite
RUN docker-php-ext-install mysqli pdo

RUN apt-get update && apt-get install -y graphviz git

RUN pecl install mongodb

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer
WORKDIR /var/www/html
RUN composer require  mongodb/mongodb

EXPOSE 80
