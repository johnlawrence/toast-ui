<?php

class AIFAspic{

	private $json;
	private $nodes = array();
	private $premises = array();
	private $ra = array();
	private $ca = array();
	private $pa = array();
	private $conclusions = array();
	private $rules = array();
	private $edges = array();
	private $contraries = array();
	
	private $rulePreferences = array();
	private $kbPreferences = array();
	private $undercutters = array();


	function AIFAspic($json){

		$this->json = $json;
		$this->processNodes();
		$this->processEdges();	
	}

	function processNodes(){
		$edges = $this->json->edges;
	
		foreach($this->json->nodes as $node){
			if($node->type=="RA"){
				$this->ra[$node->nodeID] = $node;
			}else if($node->type=="CA"){
				$this->ca[$node->nodeID] = $node;
			}else if($node->type=="PA"){
				$this->pa[$node->nodeID] = $node;
			}
		}
		foreach($this->json->nodes as $node){			
			if($node->type=="I"){

				$node->text = str_replace(" ", "_", $node->text);
				$node->text = str_replace(",", "", $node->text);
				$node->text = str_replace("'", "", $node->text);

				$this->nodes[$node->nodeID] = $node;

				$premise = true;

				foreach($edges as $edge){
					if($edge->toID==$node->nodeID && array_key_exists($edge->fromID,$this->ra)){
						$premise = false;
						break;
					}	
				}

				if($premise)
					$this->premises[] = $node;			
			}
		}
		
	}

	function processEdges(){
		foreach($this->ra as $raNode){
			$tempRule = array("antecedents" => array(), consequent=> "");

			foreach($this->json->edges as $edge){

				if($edge->toID==$raNode->nodeID){
					if(!array_key_exists($edge->fromID,$this->nodes))
						continue;

					$tempRule["antecedents"][] = $edge->fromID; //str_replace(" ", "_", $this->nodes[$edge->fromID]->text);
					foreach($this->json->edges as $edge2){
						if($edge2->fromID==$raNode->nodeID){
							if(!array_key_exists($edge2->toID,$this->nodes))
								continue;
							$tempRule["consequent"] = $edge2->toID; //str_replace(" ", "_", $this->nodes[$edge2->toID]->text);
						}
					}
				}

				
				$this->rules[$raNode->nodeID] = $tempRule;
			}
		}

//		print_r($this->rules);

		//print_r($this->ca);

		foreach($this->ca as $caNode){
			$tempContrary = array();
			$contrary = "";
		
			foreach($this->json->edges as $edge){
				if($edge->toID==$caNode->nodeID){
					if(!array_key_exists($edge->fromID,$this->nodes))
						continue;
					$tempContrary[] = $edge->fromID; //str_replace(" ", "_", $this->nodes[$edge->fromID]->text);
					foreach($this->json->edges as $edge2){
						if($edge2->fromID==$caNode->nodeID){
							if(array_key_exists($edge2->toID, $this->rules)){
								$this->undercutters[$edge2->toID] = $edge->fromID;
							}
							$contrary = $edge2->toID; //str_replace(" ", "_", $this->nodes[$edge2->toID]->text);
						}
					}
				}
				
			}
			$this->contraries[$contrary] = $tempContrary;
		}

//		print_r($this->contraries);

		foreach($this->pa as $paNode){
			foreach($this->json->edges as $edge){
				if($edge->toID==$paNode->nodeID){
					$fromID = $edge->fromID;
					//the node that this is from is more preferred
					if(array_key_exists($fromID, $this->ra)){
						//rule preference
						$pref = array(0 => $fromID, 1 => -1);
						
						foreach($this->json->edges as $edge2){
							if($edge2->fromID==$paNode->nodeID){
								//the toID of this edge is the other end of the preference
								$pref[1] = $edge2->toID;
								break;
							}
						}
						
						if($pref[1]!=-1)
							$this->rulePreferences[] = $pref;
						
					}else if(array_key_exists($fromID, $this->nodes)){
						//premise preference
						$pref = array(0 => $fromID, 1 => -1);
						
						foreach($this->json->edges as $edge2){
							if($edge2->fromID==$paNode->nodeID){
								//the toID of this edge is the other end of the preference
								$pref[1] = $edge2->toID;
								break;
							}
						}
						if($pref[1]!=-1)
							$this->kbPreferences[] = $pref;
						
					}
				}
			}
		}


	}


	function getAspicArray(){
		
		$aspicArray = array("premises" => "",
					"rules" => "",
					"contrariness" => "",
					"rulePrefs" => "",
					"kbPrefs" => "");

		$thePremises = "";		
		
		foreach($this->premises as $premise){
			$i++;
			$thePremises .= $premise->nodeID . ";\n"; //str_replace(" ", "_", $premise->text) . ";\n";
		}
		$aspicArray["premises"] = trim($thePremises);

		$i = 1;	


		$raToRuleMapping = array();

		foreach($this->rules as $ra => $rule){
			$ant = implode(",", $rule["antecedents"]);
			$cons = $rule["consequent"];

			$aspicArray["rules"] .= "[r$i] $ant=>$cons;\n";
			$raToRuleMapping[$ra] = "r$i";
			$i++;
			
			
		}

		foreach($this->undercutters as $rule=>$undercutter){
			$ruleID = $raToRuleMapping[$rule];
			$aspicArray["rules"] .= "[r$i] $undercutter=>~[$ruleID];\n";
			$i++;
		}

		$contraries = array_keys($this->contraries);

		foreach($contraries as $cont){
			foreach($this->contraries[$cont] as $cont2){
				$aspicArray["contrariness"] .= "$cont2^$cont;\n";
			}
		}
		
		foreach($this->rulePreferences as $pref){
			$aspicArray["rulePrefs"] .= $raToRuleMapping[$pref[1]] . " < " . $raToRuleMapping[$pref[0]];
		}
		
		foreach($this->kbPreferences as $pref){
			$aspicArray["kbPrefs"] .= $pref[1] . " < " . $pref[0];
		}


	
		return $aspicArray;

	}


}


?>
