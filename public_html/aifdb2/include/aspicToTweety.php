<?php

class AspicToTweety{

	var $nodes = array(), $premises = array();

	var $test = false;
	function getNodes(){
		return $this->nodes;
	}

	function getDelp($nodesetID, $escapeHTML=false){
	
		$url = "http://toast.arg-tech.org/api/aifdb/$nodesetID";

		if($this->test)
			$url .= "&test=true";
	
		$aspic = file_get_contents($url);
		$aspicJSON = json_decode($aspic);
		
		return $this->getDelp2($aspicJSON,$escapeHTML);
	
	}
	
	function getDelp2($aspicJSON, $escapeHTML=false){
		
		$ruleChar = ($escapeHTML) ? "&lt;" : "<";
		$tweetyKB = array();

		foreach(explode(";",$aspicJSON->premises) as $premise){
			$this->nodes[trim($premise)] = trim($premise);
			$this->premises[trim($premise)] = trim($premise);
		}

		/* Convert rules */
		$allConsequents = array();
		$allAntecedents = array();

		foreach(explode(";",$aspicJSON->rules) as $rule){
			if(trim($rule)!=""){
	
				$rule = preg_replace('/\[.*?\]/', "", $rule);
				
				$ruleSymbol = "";
				
				if(strpos($rule,"=>")!==false){
					$ruleParts = explode("=>",$rule);
					$ruleSymbol = "-$ruleChar";
				}else{
					$ruleParts = explode("->",$rule);
					$ruleSymbol = "$ruleChar-";
				}
		
				$cons = trim($ruleParts[1]);
				$ant = trim($ruleParts[0]);
				$consequent = array();
				$antecedent = array();
		
				foreach(explode(",",$cons) as $nodeID){
					$this->nodes[] = trim($nodeID);
					unset($this->premises[$nodeID]);
					$nodeText = $this->getNodeText(trim($nodeID));
					//$mapping["n" . $nodeText] = getNodeText2($nodeText);
					$consequent[] = $nodeText;
					$allConsequents[] = $nodeText;
				}
		
				$antecedents = array();
		
				foreach(explode(",",$ant) as $nodeID){
					$this->nodes[] = trim($nodeID);
					unset($this->premises[$nodeID]);
					$nodeText = $this->getNodeText(trim($nodeID));
					$antecedent[] = $nodeText;
					$allAntecedents[] = $nodeText;
				}
		
				$antecedents = implode(",",$antecedent);
		
				foreach($consequent as $cons){
					$tweetyKB[] = $cons . " $ruleSymbol " . $antecedents . ".";
				}
			}
		}

		/* Turn ASPIC+ contraries into Tweety rules */
		foreach(explode(";", $aspicJSON->contrariness) as $contrary){
			if(trim($contrary)!=""){
				$parts = explode("^", $contrary);
	
				$ant = trim($parts[0]);
				$cons = trim($parts[1]);
		
				unset($this->premises[$ant]);
				unset($this->premises[$cons]);
	
				$ant = $this->getNodeText($ant);
				$cons = $this->getNodeText($cons);
		
				if($ant=="" || $cons == "")
				continue;

				$allConsequents[] = "~" . $cons;
				$allAntecedents[] = $ant;
				$tweetyKB[] = "~" . $cons . " -$ruleChar " . $ant . ".";
			}
		}



		foreach($allAntecedents as $ant){
			if(!in_array($ant, $allConsequents) && !in_array("~" . $ant, $allConsequents) && !in_array($ant . ".",$tweetyKB))
				$tweetyKB[] = $ant . ".";
		}

		foreach($this->premises as $premise){
			$premise = trim($premise);
			if($premise=="")
				continue;
		
			$tweetyKB[] = "n" . $premise . ".";
		}

		return $tweetyKB;
	
	}	


	function getNodeText2($nodeID){
		$node = json_decode(file_get_contents("http://arg-tech.org/AIFdb/nodes/$nodeID"));
	
		//print_r($node);
		//echo $node->nodeID . ": " . $node->text . "<br />";

	
	
	
		$text = trim($node->text);
		$text = str_replace('â€™',"",$text);
		$text = preg_replace('/\{.*?\}/', "", $text);
	
		$text = preg_replace('/\[.*?\]/', "$1", $text);
		$text = preg_replace('/\(.*?\)/', "$1", $text);
	
		$text = str_replace(",","",$text);
		$text = str_replace("?","",$text);
		$text = str_replace(".","",$text);
		$text = str_replace("!","",$text);
		$text = str_replace(":","",$text);
		$text = trim(trim($text, "."));
		$text = str_replace(" ","_",$text);
		$text = str_replace("'","",$text);
		$text = str_replace('"',"",$text);
	
		//echo $node->nodeID;// . ": " . $text . "<br />";
	
		return strtolower($text);
	}



	function getNodeText($nodeID){
		$node = json_decode(file_get_contents("http://arg-tech.org/AIFdb/nodes/$nodeID"));
	
		//print_r($node);
		//echo $node->nodeID . ": " . $node->text . "<br />";
	
		return "n" . $nodeID;
	
	
	
		$text = trim($node->text);
		$text = str_replace('â€™',"",$text);
		$text = preg_replace('/\{.*?\}/', "", $text);
	
		$text = preg_replace('/\[.*?\]/', "$1", $text);
		$text = preg_replace('/\(.*?\)/', "$1", $text);
	
		$text = str_replace(",","",$text);
		$text = str_replace("?","",$text);
		$text = str_replace(".","",$text);
		$text = str_replace("!","",$text);
		$text = str_replace(":","",$text);
		$text = trim(trim($text, "."));
		$text = str_replace(" ","_",$text);
		$text = str_replace("'","",$text);
		$text = str_replace('"',"",$text);
	
		//echo $node->nodeID;// . ": " . $text . "<br />";
	
		return strtolower($text);
	}
}



?>
