<?php 
	include "include/aspicToTweety.php";
	$nodeset = (isset($_GET['nodeset'])) ? $_GET['nodeset'] : -1;
	
	if($nodeset!=-1){
	
		$semantics = (trim($_GET['semantics'])!="") ? $_GET['semantics'] : "grounded";
		
		
		$selectedExtension = 1;
	}
	
	header("Content-Type:text/html");
?>
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8" />
                <title>TOAST@AIFdb: Abstract Argumentation Frameworks for AIFdb</title>
                <script type="text/javascript" src="/aifdb2/include/scripts/jquery.js"></script>
                <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
  				<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
                <script type="text/javascript" src="/aifdb2/include/scripts/jquery.form.js"></script>
                <script type="text/javascript" src="/aifdb2/include/scripts/jquery.tooltip.js"></script>
                <script type="text/javascript" src="/aifdb2/include/scripts/tweetyaif.core.js"></script>
                <script type="text/javascript">
					var _gaq = _gaq || [];
			  		_gaq.push(['_setAccount', 'UA-10861232-3']);
			  		_gaq.push(['_trackPageview']);

			  		(function() {
			    		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			    		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			    		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			  		})();
				</script>

                <?php
	                if($nodeset!=-1){
	            ?>
	            
                <script type="text/javascript">
                	$(document).ready(function(){
                		/*var width = $("#outer").width();
                		var src = "/image/<?php echo $nodeset; ?>_<?php echo $semantics; ?>_<?php echo $selectedExtension; ?>.png";
                		var link = $('<a>').attr("href",src).attr("target","_new");
						var img = $("<img />").attr("id","res_img").attr("src",src + '?width=' + width).attr("alt","Nodeset 29")
									.on("load",function(){
										$("#image_loading").fadeOut("slow",function(){
											$("#result_image").append(link);
											$("#result_image").fadeIn("slow");
										});
									}).css("margin","auto").css("left",0).css("right",0).appendTo(link);*/
									
									$( "#tabs" ).tabs();
									
					});
                </script>
                <?php
                	}
                ?>
             
                <link rel="stylesheet" href="/aifdb2/include/style/style.css" />
        </head>
        <body>
                <div id="ccol">
                        <header>
                        	<span style="position:relative;">
                        		<img src="/aifdb2/include/img/aifdb-toast.png" style="height:65px;" alt="TOAST@AIFdb" /><br />
                        		<span class="logo">
                        			<a href="">TOAST</a>@<a href="">AIFdb</a>
                        		</span>
                        	</span>
                        	<br />
                        </header>

