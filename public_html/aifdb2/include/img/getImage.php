<?php

$arguments = $_POST['arguments'];
$extensions = $_POST['extensions'];
$acceptableConclusions = $_POST['acceptableConclusions'];
$defeat = $_POST['defeat'];

$ext = $_GET['extension'] - 1;

$extLabels = array();
foreach($extensions[$ext] as $arg){
	$extLabels[] = getArgumentLabel($arg);
}

//print_r($_POST);

eval('echo ' . $_GET['type'] . 'Framework();');


function abstractFramework(){

	global $arguments, $extensions, $acceptableConclusions, $defeat, $ext, $extLabels;

	$islands = ($_GET['islands']=="1") ? true : false;

	$nonIslands = array();

	foreach($defeat as $def){
		$splitDefeat = explode(">",$def);
		$nonIslands[] = trim($splitDefeat[0]);
		$nonIslands[] = trim($splitDefeat[1]);
	}

	$theDot = "digraph framework{ bgcolor = \"#EEEEEE\"; ";

	foreach($arguments as $arg){
		$label = getArgumentLabel($arg);
		if(in_array($label,$extLabels)){
			if(!$islands && !in_array($label, $nonIslands))
				continue;
		
			$theDot .= "$label [style=filled,color=\"#008C00\"]; ";
		}else{
			$theDot .= "$label [style=filled,color=\"#B02B2C\"]; ";
		}
	}

	foreach($defeat as $def){
		$theDot .= str_replace(">","->",$def) . "; ";
	}

	$theDot .= " rankdir=\"LR\"; }";

	$theDot = addslashes($theDot);

	return getImageBytes($theDot);

}

function structuredFramework(){
	global $arguments, $extensions, $acceptableConclusions, $defeat, $ext, $extLabels;
	
	$nodeset = $_GET['nodeset'];
	
	$aif = json_decode(file_get_contents("http://arg-tech.org/AIFdb/json/$nodeset"));
	
	$theDot = "digraph framework{ bgcolor = \"#EEEEEE\"; ";
	
	$dotNodes = array();
	
	foreach($aif->nodes as $node){
		$nodeID = $node->nodeID;
		$text = $node->text;
		switch($node->type){
			case "I":
				$color = (in_array($nodeID,$acceptableConclusions[$ext])) ? "#008C00" : "#B02B2C";
				$text = wordwrap(str_replace('"','\"',$text),30,'\n',true);
				$dotNodes[$nodeID] = $nodeID . " [shape=\"box\",fillcolor=\"#E8F0FF\",style=\"solid,filled\",color=\"$color\",label=\"$text\"]";
				break;
			
			case "RA":
				$dotNodes[$nodeID] = $nodeID . '[style="filled",color="#BF5153",fillcolor="#E2FFE2",shape="diamond",label="' . $text . '"]';
				break;
			
			case "CA":
				$dotNodes[$nodeID] = $nodeID . '[style="filled",color="#BF5153",fillcolor="#FFE1E2",shape="diamond",label="' . $text . '"]';
				break;
		
		}
	}
	
	$theDot .= implode("; ", $dotNodes);
	
	$dotNodeIDs = array_keys($dotNodes);
	
	foreach($aif->edges as $edge){
		if(in_array($edge->fromID, $dotNodeIDs) || in_array($edge->toID, $dotNodeIDs)){
			$theDot .= $edge->fromID . "->" . $edge->toID . "; ";
		}
	}
	
	$theDot .= "}";
	$theDot = addslashes($theDot);
	
	return getImageBytes($theDot);
	

}


function getImageBytes($theDot){
	ob_start();
	 	passthru("/home/mark/public_html/tweety/createImg.sh \"$theDot\"");
		$imageBytes = ob_get_contents();
	ob_end_clean();

	return $imageBytes = base64_encode($imageBytes);
}

function getArgumentLabel($argument){
	$argAndLabel = explode(":",$argument);
	return trim($argAndLabel[0]);
}



?>

