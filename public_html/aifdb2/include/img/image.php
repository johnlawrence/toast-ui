<?php
	header("content-type:image/png");


	$nodeset = $_GET['nodesetID'];
	$semantics = str_replace("-","",$_GET['semantics']);
	$extension = $_GET['extension'];

	$aif = json_decode(file_get_contents("http://arg-tech.org/AIFdb/json/$nodeset"));
	
	$json = json_decode(file_get_contents("http://ws.arg.tech/t/aifdb/$nodeset-aspic/$semantics"));

	$arguments = $json->arguments;
	$atts = $json->defeat;

	$conclusions = array();
	$labels = array();

	$newArgs = array();
	
	
	$argsWithInteractions = array();
	
	foreach($atts as $att){
		$argsWithInteractions = array_merge(explode(">",$att),$argsWithInteractions);
	}
	
	
	foreach($arguments as $arg){
		$argLabel = explode(":",$arg);
		$newArgs[] = $argLabel[0];

		if(strpos($argLabel[1],"=>")===false){
			$conclusions[$argLabel[0]] = trim($argLabel[1]);
			$labels[trim($argLabel[1])] = $argLabel[0];
		}else{
			$conclusion = explode("=>",$argLabel[1]);
			$conclusions[$argLabel[0]] = trim($conclusion[1]);
			$labels[trim($conclusion[1])] = $argLabel[0];
		}
	}
	
	$oldExt = $json->extensions[$extension-1];
	
	$ext = array();
	
	foreach($oldExt as $arg){
		$split = explode(":",$arg);
		$ext[] = trim($split[0]);
	}
	

	$newAtts = array();
	foreach($atts as $att){
		$newAtts[] = "(" . str_replace(">",",",$att) . ")";
	}
	//print_r($response);
	
	
	if($_GET['structured']=="true"){
		
		$nodes = $aif->nodes;
		
		$dotNodes = array();
		
		$includedNodes = array();
		
		foreach($nodes as $node){
			if($node->type=="I"){
				$includedNodes[] = $node->nodeID;
				$label = $labels[$node->nodeID];
				$color = (in_array($label,$ext)) ? "green" : "red";
				
				$text = wordwrap(str_replace('"','\"', $node->text),30,'\n',true);
				
				
				$dotNodes[] = $node->nodeID . " [shape=\"box\",fillcolor=\"#E8F0FF\",style=\"solid,filled\",color=\"$color\",label=\"$text\"];";
			}else if($node->type=="CA"){
				$includedNodes[] = $node->nodeID;
				
				$text = ($node->scheme!="") ? $node->scheme : $node->text;
				
				//$text = $node->text;
				$dotNodes[] = $node->nodeID . " [style=\"filled\",color=\"#BF5153\",fillcolor=\"#FFE1E2\",shape=\"diamond\",label=\"$text\"]";
			}else if($node->type=="RA"){
				$includedNodes[] = $node->nodeID;
				$text = ($node->scheme!="") ? $node->scheme : $node->text;
				//$text = $node->text;
				$dotNodes[] = $node->nodeID . " [style=\"filled\",color=\"#58C653\",fillcolor=\"#E2FFE2\",shape=\"diamond\",label=\"$text\"]";
			}
		}
		
		$dotEdges = array();
		
		foreach($aif->edges as $edge){
			$from = $edge->fromID;
			$to = $edge->toID;
			
			if(in_array($from,$includedNodes) && in_array($to,$includedNodes)){
				$dotEdges[] = "$from -> $to;";
			}
		}
	
	
		$theDot = "digraph framework{\n" . implode($dotNodes,"\n") . "\n" . implode($dotEdges,"\n") . "}";
		
		$theDot = addslashes($theDot);
	}else{
		$theDot = "digraph framework{ bgcolor = \"transparent\";\n";
	
		$islands = !($_GET['islands']=="false");
		
		foreach($newArgs as $arg){
			if(!$islands){
				if(!in_array($arg,$argsWithInteractions))
					continue;
			}
		
			$color = (in_array($arg,$ext)) ? "green" : "red";
			$theDot .= "$arg [shape=\"circle\",fillcolor=\"$color\",style=\"filled\"];\n";
		}
		
		foreach($json->defeat as $att){
			$att = str_replace(">","->",$att) . ";\n";
			$theDot .= $att;
		}
	
		$theDot .= "}";
	}

	
	ob_start();
   		passthru("/home/mark/public_html/tweety/createImg.sh \"$theDot\"");
		$imageBytes = ob_get_contents();
   	ob_end_clean();
   	
   	if(isset($_GET['width'])){
   		$width = $_GET['width'];
   		$img = new Imagick();
   	
   		$img->readImageBlob($imageBytes);
   	
   		if($img->getImageWidth() > $width){
   			$img->thumbnailImage($width,0);
   		}
   	
   		echo $img->getImageBlob();
   	}else{
   		echo $imageBytes;
   	}
?>
