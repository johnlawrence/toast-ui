<?php



error_reporting(0);
//ini_set('display_errors', 'on');
session_start();
$id = -1;;




if($_GET['example']=="true")
	echo <<< EOT
		{"premises":"Snores; Professor;",
		"kbPrefs":"Snores < Professor;",
		"rules":"[r1] Snores=>Misbehaves; [r2] Misbehaves=>AccessDenied; [r3] Professor=>AccessAllowed;",
		"rulePrefs":"[r1] < [r2]; [r1] < [r3]; [r3] < [r2];",
		"contrariness":"AccessDenied-AccessAllowed",
		"axioms":"",
		"assumptions":"",
		"semantics":"grounded",
		"query":"AccessDenied"}
EOT;
else{
	$result = getResult();
	echo getResults($result);
}

function getResults($result){	

	global $id;
	$toReturn = "";
	
	$showIslands = ($_POST['islands']=="true") ? true : false;
	
	$jsonText = json_encode($result);
	$nodeset = $_GET['nodeset'];
	
	$acceptableJS = "";
	
	foreach($result->acceptableConclusions as $id => $acceptable){
		$acceptableJS .= " acceptableConclusions[$id] = ['" . implode("','",$acceptable) . "'];";
	}
	
	
	

	echo <<< EOT
		<script type="text/javascript">
		
			var acceptableConclusions = new Array();
			
			{$acceptableJS}
			
			var whichFramework = 'abstract';
			var imageCache;
		
			$(document).ready(function(){
				imageCache = new Object();
				imageCache['abstract'] = new Object();
				imageCache['structured'] = new Object();
				getImage(1);
			});
			
			function showHide(container){
				var display = $('#' + container).css('display');
				var word = "Show";
				
				if(display=="block"){
					display = "none";
				}else{
					display = "block";
					word = "Hide";
				}
				
				$('#' + container).css('display', display);
				$('#' + container + '_showhide').html(word);
				
				
				return false;
			}
			
			function checkImageLoaded(img){
				if(!img.complete){
					setTimeout(checkImageLoaded, 10, img);
				}
			}	
		
			function getImage(ext){
			
				var img = new Image();
				
				$(".arglink").each(function(){
					if($.inArray($(this).attr("id"),acceptableConclusions[ext-1])==-1){
						$(this).css("color","red");
					}else{
						$(this).css("color","green");
					}
				});
				
				$(img).on('load',function(){
						setTimeout(checkImageLoaded, 10, img);
						$("#img_ext").attr("src",img.src);
						
						var w;
						
						if(img.width > 700){
							w = 700;
						}else{
							w = img.width;
						}
						
						$("#img_ext").css("width", w + "px").css("height","100%");
							
						$("#img_ext_link").attr("href",img.src);
							
						//getImage(ext);
						
					});
			
				if(typeof imageCache[whichFramework][ext]!='undefined'){
					
					var w;
					img.src = imageCache[whichFramework][ext];
				}else{
					$("#img_ext").attr('src','/include/img/loader-larger.gif').css('height','100px').css('width','100px');
					
					var url = "/include/img/getImage.php?type=" + whichFramework + "&islands={$showIslands}&extension=" + ext;
			
					if(whichFramework=='structured'){
						url += '&nodeset={$nodeset}';
					}
			
					$.ajax({
						type: "POST",
						url: url,
						data: {$jsonText},
						success: function(response){
						//alert(response);
						
							var w;
							
							img.src = imageCache[whichFramework][ext] = 'data:image/png;base64,' + response;
							
							
							
							
						}
					});
				}
			}
			
			function showFramework(which){
				whichFramework = which;
				getImage(1);
				return false;
			}
		</script>
EOT;

	/*$argLabels = array();
	$defeat = array();

	unset($_SESSION['framework']);

	foreach($result->arguments as $arg){
		$label = explode(":",$arg);
		$argLabels[] = $label[0];
	}

	foreach($result->defeat as $def){
		$defeat[] = "(" . str_replace(">",",",$def) . ")";
	}

	$framework = array("arguments" => $argLabels, "defeat" => $defeat);
	$_SESSION['framework'] = $framework;
	
	$showIslands = ($_POST['islands']=="true") ? true : false;
	$images = getImages($result->arguments, $result->defeat, $result->extensions, $showIslands);*/

	if(!$result->wellformed){
		$toReturn .= "<b>WARNING: the argumentation theory is not well-formed (<a onclick=\"return false;\" href=\"\" class=\"wellFormedWhy\" alt=\"" . $result->wellFormedReason . "\">why?</a>)</b><br /><br />";
	}

	$toReturn .= "<b>" . $result->result . "</b><br /><br />";

	$aifdb = ($_POST['aifdb']=="true") ? true : false;
	
	$toReturn .= "<div id=\"frameworkContainer\">";

	$toReturn .= "<div id=\"arguments_container\"><div class=\"container_topbar\" id=\"arguments_container_topbar\">Arguments (<a href=\"\" onclick=\"return showHide('arguments');\"><span id=\"arguments_showhide\">Hide</span></a>)</div><br /><div id=\"arguments\" style=\"display:block;\"><ul>";

	$args = $result->arguments;
	natsort($args);


	$extensions = !empty($result->extensions);

	$tooltips = array();

	foreach($args as $arg){
		$splitArg = explode(":",$arg);
		$splitArg2 = explode(">",$splitArg[1]);

		$size = sizeof($splitArg2);
		
		$newArg = $splitArg[0] . ": ";
		
		if($size > 1){
			$argId = trim($splitArg2[1]);
			$newArg .= $splitArg2[0] . "&gt;";
		}else{
			$argId = trim($splitArg2[0]);
		}
			
		$link = ($aifdb) ? " href=\"http://www.arg.dundee.ac.uk/AIFdb/nodeview/$argId\" target=\"_new\"" : "";
			
		$newArg .= "<a id=\"$argId\"$link class=\"arglink\" style=\"cursor:hand;\" alt=\"$argId\">" . $argId . "</a>";

		$toReturn .= "<li>" . $newArg . "</li>";
	}


	$toReturn .= "";

	$img1 = $images[0]["image"];
	$ext1 = $images[0]["extension"];

	$tempExt = array();

	foreach($ext1 as $arg){
		$exploded = explode(":",$arg);
		$tempExt[] = trim($exploded[0]);
	}
	
	$first = true;
	
	$toggleLinks = array();
	$i = 1;
	
	$ext = ($extensions) ? "{" . implode(", ",$ext) . "}" : "<br />There are no extensions";
	
	for($i=0;$i<sizeof($result->extensions);$i++){
		$j = $i+1;
		$toggleLinks[] = "<a href=\"javascript:getImage($j); void(0);\">$j</a>";
	}
	
	$toggle = "";
	$diagram = "";
	
	if($aifdb){
		$diagram = "<br /><br />Show diagram: <a href=\"\" onclick=\"return showFramework('abstract');\">Abstract framework</a> | <a href=\"\" onclick=\"return showFramework('structured');\">Structured framework</a><br /><br />";
	}
	
	
	if($extensions){
		$toggle = "<br /><br />Show extension: " . implode(" ",$toggleLinks);
	}else{
		$toggle = $ext;
	}
	
	$toReturn .= "</ul></div></div><br /><br /><div id=\"container_img\"><div class=\"container_topbar\" id=\"img_container_topbar\">Extensions (<a href=\"\" onclick=\"return showHide('img');\"><span id=\"img_showhide\">Hide</span></a>)</div><br /><div id=\"img\"><a id=\"img_ext_link\"><img id=\"img_ext\" src=\"/include/img/loader-larger.gif\" style=\"display:block; margin-left:auto; margin-right:auto; height:100px;\" /></a>$toggle $diagram</div></div><br /><br />";
	
	
	/*foreach($images as $image){
		$img = $image["image"];
		
		$ext = array();
		
		foreach($image["extension"] as $arg){
			$temp = explode(":",$arg);
			$ext[] = trim($temp[0]);
		}
		

		
		$ext = ($extensions) ? "{" . implode(", ",$ext) . "}" : "There are no extensions";
		
		
		$extra = "";
		
		if($first){
			$first = false;
			$extra = " style=\"display:block; position:relative; top:2px;\"";
		}else{
			$extra = " style=\"display:none; position:relative;	top:2px;\"";
		}
		
		$toReturn .= <<< EOT
			<div id="{$img}" class="resultImage"{$extra}>
				<a href="/include/img/extension.php?ext=$img" target="_new"><img src="/include/img/extension.php?ext=$img" /></a><br /><br />{$ext}
			</div>
EOT;
		$toggleLinks[] = <<< EOT
			<a href="javascript:showExtension('{$img}'); void(0);">{$i}</a>
EOT;
		$i++;
	}
	$toReturn .= "</div>";*/

	$toReturn .= "</div>";

	

	



	//		The framework is:<br /><br />$image<br /><br />
/*	$toReturn .= "<br /><br />The extension(s) are:<ol>";

	$extensions = false;

	foreach($result->extensions as $ext){
	$extensions = true;
	$toReturn .= "<li>{";
		asort($ext);
		print_r($ext);

		for($i=0;$i<sizeof($ext);$i++){
			$arg = explode(":",$ext[$i]);
			$toReturn .= trim($arg[0]);

			if($i!=sizeOf($ext)-1)
				$toReturn .= ", ";
		}
		$toReturn .= "}</li>";
	}
	if(!$extensions)
		$toReturn .= "<li>There are no extensions</li>";	

	$toReturn .= "</ol><br />";*/
$toReturn .= "<span style=\"font-size:12pt;\">Share by using this URL: <a href=\"http://toast.arg-tech.org/$id\" target=\"_new\">http://toast.arg-tech.org/$id</a></span>";
	return $toReturn;
}


function getImages($args, $def, $ext, $showIslands=true){
	asort($ext);

	$_SESSION['img'] = array();


	$i = 0;
	$images = array();

	if(empty($ext))
		$ext = array(array());

	foreach($ext as $extension){
		$i++;
		$dot = createDot($args,$def,$extension,$showIslands);
		$filename = md5(time("d/m/y") . $i);

		$myfile = "/home/toast/public_html/temp/$filename.dot";
		$fh = fopen($myfile, 'w');
		fwrite($fh, $dot);
		fclose($fh);

		$out = exec("dot -Tpng $myfile -o /home/toast/public_html/temp/$filename.png",$output);
		$img = file_get_contents("/home/toast/public_html/temp/$filename.png");
		exec("rm /home/toast/public_html/temp/$filename.png");

		unset($_SESSION['img'][$i]);
		$_SESSION['img'][$i] = $img;

		exec("rm /home/toast/public_html/temp/$filename.dot");
		$images[] = array("image" => $i,
					"extension" => $extension);
	}

	return $images;
	//return '<img src="temp/' . $filename . '.png" alt="Abstract framework" />';
}

function getResult(){
	global $id;
//	$req = new HttpRequest("http://ova.computing.dundee.ac.uk:8080/AspicOnline2/proc",HttpRequest::METH_POST);

	$output = <<< EOT

	array("axioms" => "$_POST[txtAxioms]",
		"assumptions" => "$_POST[assumptions]",
		"premises" => "$_POST[premises]",
		"rules" => "$_POST[rules]",
		"kbPrefs" => "$_POST[kbprefs]",
		"rulePrefs" => "$_POST[ruleprefs]",
		"query" => "$_POST[query]",
		"link" => "$_POST[link]",
		"contrariness" => "$_POST[contrariness]",
		"language" => "$_POST[language]",
		"semantics" => "$_POST[semantics]",
		"transposition" => "$_POST[transposition]");
EOT;

		$output = addslashes($output);
		$conn = mysql_connect("localhost","toast_user","toastpa55");
		mysql_select_db("toast",$conn);

		$result = mysql_query("SELECT argID FROM arguments WHERE data='$output';");

	

		if($result){
			while($row = mysql_fetch_assoc($result))
				$id = $row["argID"];
		}
		if($id==-1){
			mysql_query("INSERT INTO arguments(data) VALUES('$output');");
			$id = mysql_insert_id();
		}
		
		

		mysql_close($conn);
		

		//file_put_contents("saved/test1.txt",$output);


	$fields = array("axioms" => $_POST['txtAxioms'],
		"assumptions" => $_POST['assumptions'],
		"premises" => $_POST['premises'],
		"rules" => $_POST['rules'],
		"kbPrefs" => $_POST['kbprefs'],
		"rulePrefs" => $_POST['ruleprefs'],
		"query" => $_POST['query'],
		"link" => $_POST['link'],
		"contrariness" => $_POST['contrariness'],
		"language" => $_POST['language'],
		"semantics" => $_POST['semantics'],
		"transposition" => $_POST['transposition']);


//	if($fields["query"]=="")
//		$fields["query"] = " ";

	$data = "";

	foreach($fields as $field => $value)
		$data .= "$field=$value&";

	$data = rtrim($data,"&");

//	echo $data;
//	ech

//	$req->addPostFields($fields);
//	$req->send();	
//	$response = $req->getResponseBody();

	
	$ch = curl_init();
	//curl_setopt($ch, CURLOPT_URL, "http://ova.computing.dundee.ac.uk:8080/AspicOnline2/proc");
	curl_setopt($ch, CURLOPT_URL, "http://haecceity.computing.dundee.ac.uk:8080/AspicOnline2/proc");
	curl_setopt($ch, CURLOPT_POST, count($fields));
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$response = curl_exec($ch);


//	echo $response; //debug
	$theResponse = json_decode($response);
	//print_r($theResponse); //debug

	return $theResponse;

//	return $req->getResponseBody();
}

function createDot($args,$def,$ext,$showIslands=true){

	$acceptableArgs = array();

	foreach($ext as $arg){
		$argAndLabel = explode(":",$arg);
		$acceptableArgs[] = trim($argAndLabel[0]);
	}

//print_r($def);


	$argLabels = array();
	foreach($args as $arg){
		$split = explode(":",$arg);
		$argLabels[] = trim($split[0]);
	}


	$islands = array();

	$inDef = false;
	$currentLabel = "";

	for($i=0;$i<sizeof($argLabels);$i++){
		$inDef = true;
		for($j=0;$j<sizeof($def);$j++){						
			if(strpos($def[$j],$argLabels[$i])===false){
				$inDef = false;
			}
		}
		if(!$inDef)
			$islands[] = $argLabels[$i];
	}



	$theDot = "digraph framework {\n bgcolor = \"#EEEEEE\";\n";

	$proceed = true;

	foreach($argLabels as $arg){

		$proceed = true;
		//if(!$showIslands){
		//	$proceed = !in_array($arg,$islands);				
	//	}
		if($showIslands){
			if($proceed){
				$theDot .= $arg;
				if(in_array($arg,$acceptableArgs)){
					$theDot .= "[style=filled,color=\"#008C00\"]";
				}else{
					$theDot .= "[style=filled,color=\"#B02B2C\"]";
				}
				$theDot .= "\n";
			}
		}
		$theDot .= "rankdir=\"LR\";\n";
	}

	for($i=0;$i<sizeof($def);$i++){		
		$split = explode(">",$def[$i]);
		if(!$showIslands){

			$theDot .= $split[0];

			if(in_array($split[0],$acceptableArgs)){
				$theDot .= "[style=filled,color=\"#008C00\"]";
			}else{
				$theDot .= "[style=filled,color=\"#B02B2C\"]";
			}
			$theDot .= "\n" . $split[1];
			
			if(in_array($split[1],$acceptableArgs)){
				$theDot .= "[style=filled,color=\"#008C00\"]";
			}else{
				$theDot .= "[style=filled,color=\"#B02B2C\"]";
			}
		}

		$theDot .= $split[0] . "->" . $split[1];
		
		if($i != sizeof($def)-1)
			$theDot .= ",";

		$theDot .= "\n";
	}

	$theDot .= "}";

//	echo $theDot;

	return $theDot;
}		
?>
