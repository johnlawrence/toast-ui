			<div class="container">
				<div id="topmsg">
			TOAST@AIFdb connects to the <a href="http://toast.arg.tech">TOAST API</a> to first translate <a href="http://aifdb.org">AIFdb</a> nodesets into <a href="">ASPIC<sup>+</sup></a>, then evaluate them under Dung-style semantics. For details see the main reference:
			
			<p><i>M. Snaith and C. Reed. TOAST: online ASPIC+ implementation. In Proceedings of the Fourth International Conference on Computational Models of Argument (COMMA 2012), 2012. [<a href="http://toast.arg-tech.org/snaith-reed-comma12.pdf">pdf</a>]</i>.
				</div>
				<?php
				$r = explode("/",$_SERVER['REQUEST_URI']);
				
				//print_r($_SERVER);
				
				$semantics = $_GET['semantics'];
				
				unset($r[sizeof($r)-2]);
				$dir = implode("/",$r) . "/";
				
				$dir = $_SERVER['SERVER_NAME'];
				
					if($nodeset==-1){
						echo <<< EOT
						<form method="POST" onsubmit="location.href='http://toast.arg.tech/' + $('#nodeset').val() + '/' + $('#semantics').find(':selected').val(); return false;">
							<span style="font-size:10pt;">
								Please either visit <a href="http://aifdb.org">aifdb.org</a> and choose a nodeset to evaluate, or enter a nodset ID here:
								<input type="text" id="nodeset" name="nodeset" />
								<select name="semantics" id="semantics">
									<option value="grounded">Grounded</option>
									<option value="preferred">Preferred</option>
								</select>
								<input type="submit" value="Go"/>
							</span>
						</form>
						
EOT;
					}else{
				
				?>
							<br />
							<span class="secthead">
								Result for <a href="http://aifdb.org/argview/<?php echo $nodeset ?>">Nodeset 
								<?php echo $nodeset ?></a> under <?php echo ($semantics=="semistable") ? "semi-stable" : $semantics; ?> semantics
								<span style="float:right">
								<select name="semantics" onchange="location.href='http://toast.arg.tech/aifdb2/<?php echo $nodeset; ?>/' + $(this).find(':selected').val(); return false;">
								
								<option value="<?php echo $semantics; ?>">Choose another semantics</option>
								<option value="grounded">Grounded</option>
								<option value="preferred">Preferred</option>
								<option value="stable">Stable</option>
								<option value="semistable">Semi-stable</option>
								</select>
								
								</span>
								
							</span><br /><br />
							
							<?php
								$json = json_decode(file_get_contents("http://ws.arg.tech/t/aifdb/$nodeset-aspic/$semantics"));

								$arguments = $json->arguments;
								$atts = $json->defeat;
								
								$extensions = $json->extensions;

								$conclusions = array();

								foreach($arguments as $arg){
									$argLabel = explode(":",$arg);
									
									/* If arg has a rule, the conclusion if after it */
									if(strpos($argLabel[1],"=>")===false){
										$conclusions[$argLabel[0]] = trim($argLabel[1]);
									}else{
										$conclusion = explode("=>",$argLabel[1]);
										$conclusions[$argLabel[0]] = trim($conclusion[1]);
									}
								}

								
								
								echo "<div id=\"tabs\">
											<ul>";
								
								for($i=1;$i<sizeof($extensions)+1;$i++){
									echo "<li><a href=\"#tabs-$i\">" . ucfirst($semantics) . " extension";
									echo (sizeof($extensions) > 1) ? " $i</a>" : "</a>";
									echo "</li>";
								}
								
								echo "</ul>";
									
								$i = 1;	
									
								foreach($extensions as $ext){
									echo "<div id=\"tabs-$i\">";
										
									$acceptable = array();
									$unacceptable = array();
								
									foreach($arguments as $arg){
									
										$split = (strpos($arg,"=>")==false) ? explode(":",$arg) : explode("=>",$arg);
										
										$node = json_decode(file_get_contents("http://aifdb.org/nodes/" . trim($split[1])));
										$conclusion = $node->text;
									
										if(in_array($arg,$ext)){
											$acceptable[] = '<div class="arg_acceptable">' . $conclusion . '</div>';
										}else{
											$unacceptable[] = '<div class="arg_unacceptable">' . $conclusion . '</div>';
										}
									}
								
									echo "<span class=\"secthead\">ASPIC+ program (<a href=\"\" onclick=\"return showHide('aspic',this);\">Hide</a>)</span><hr /><br /><div id=\"aspic\">";
									
									echo "<span id=\"premises_title\" class=\"title\">Premises:
											<div id=\"premises\" class=\"aspic_kb\">";
											
									$premises = explode(";",$json->premises);
									
									for($j=0;$j<sizeof($premises);$j++){
										if($premises[$j]==""){
											unset($premises[$j]);
										}else{
											$premises[$j] = "node_" . $premises[$j];
										}
									}
									
									echo implode($premises,";<br />") . ";";
									
									echo "</div></span>";
									
									echo "<span id=\"rules_title\" class=\"title\">Rules:
											<div id=\"rules\" class=\"aspic_kb\">";
											
									$rules = explode(";",$json->rules);
									
									echo str_replace("=>","=>node_",str_replace(",",",node_",str_replace("] ", "] node_",implode($rules,";<br />"))));
									
									echo "</div></span>";
									
									echo "<span id=\"cont_title\" class=\"title\">Contrariness:
											<div id=\"cont\" class=\"aspic_kb\">";
											
									$cont = explode(";",$json->contrariness);
									
									for($j=0;$j<sizeof($cont);$j++){
										if($cont[$j]==""){
											unset($cont[$j]);
										}else{
											$cont[$j] = "node_" . str_replace("^","^node_",$cont[$j]);
										}
									}
									
									echo implode($cont,";<br />") . ";";
									
									echo "</div></span></div><br /><br />";
									
								
								
									echo "<span class=\"secthead\">Acceptable conclusions
											(<a href=\"\" onclick=\"return showHide('acceptable_args',this);\">Hide</a>)</span><hr />
											<span id=\"acceptable_args\" class=\"conc\">";
									
									echo implode($acceptable,"<br />");
									
									echo "</span><br /><br /><span class=\"secthead\">Unacceptable or undecided conclusions 
										 (<a href=\"\" onclick=\"return showHide('unacceptable_args',this);\">Hide</a>)</span><hr />
										 <span id=\"unacceptable_args\" class=\"conc\">";
										 
									echo implode($unacceptable,"<br />");
									
									echo "</span><br /><br />";
									
									echo "<span class=\"secthead\">
											Abstract Argumentation Framework 
											(<a href=\"\" onclick=\"return showHide('result_image_$i',this);\">Hide</a>)
											(<a href=\"\" id=\"switch_$i\">Show framework without islands</a>)
										  </span>";
										  
									echo <<< EOT
											<script type="text/javascript">
											
											function load_images_{$i}(){
												var img = $('<img />').attr("src","/aifdb2/image/{$nodeset}_{$semantics}_{$i}.png?width=850").on('load',function(){
													$("#result_image_{$i}").fadeTo(250,0,function(){
														$("#result_image_{$i}").html("");
														$("#result_image_{$i}").append(img);
														$("#result_image_{$i}").fadeTo(250,1);												
													});
												});
												
												var img0 = $('<img />').attr("src","/aifdb2/image/{$nodeset}_{$semantics}_{$i}.png?width=850&islands=false").on('load',function(){
												
															$("#switch_$i").on('click',function(){
																var oldImg = $("#result_image_{$i} > img")[0];
																var newImg;
														
																if(img.attr("src")==$(oldImg).attr("src")){
																	$(this).html('Show framework with islands');
																	newImg = img0;
																}else{
																	newImg = img;
																	$(this).html('Show framework without islands');
																}
														
																$("#result_image_{$i}").fadeTo(250,0,function(){
																	//$("#result_image_{$i}").html("");
																	$("#result_image_{$i}").html(newImg);
																	$("#result_image_{$i}").fadeTo(250,1);												
																});
																return false;
															});
														});
											
												var img2 = $('<img />').attr("src","/aifdb2/image/{$nodeset}_{$semantics}_{$i}.png?structured=true&width=850").on('load',function(){
													$("#result_image_s_{$i}").fadeTo(250,0,function(){
														$("#result_image_s_{$i}").html("");
														$("#result_image_s_{$i}").append(img2);
														$("#result_image_s_{$i}").fadeTo(250,1);												
													});
												});
											}
											load_images_{$i}();
											</script>								
EOT;
										
										
									echo "<hr />
											<span id=\"outer\" style=\"display:block; width:958px; text-align:center;\">
												<span id=\"result_image_$i\">
													<!--<img src=\"/image/{$nodeset}_{$semantics}_{$i}.png?width=850\" />-->
													<img src=\"/include/img/loader-larger.gif\" class=\"loading_img\" />
												</span>
											</span><br /><br />";
									
									echo "<span class=\"secthead\">
											Structured Argumentation Framework (<a href=\"\" onclick=\"return showHide('result_image_s_$i',this);\">Hide</a>)
										</span>";
										
									echo "<hr /><span id=\"outer2\" style=\"display:block; width:958px; text-align:center\">
											<span id=\"result_image_s_$i\">
												<!--<img src=\"/image/{$nodeset}_{$semantics}_{$i}.png?structured=true&width=850\" />-->
												<img src=\"/include/img/loader-larger.gif\" class=\"loading_img\" />
											</span>
										</span>";
										  
										  
										  
									
									/* Closing div for the tab */
									echo "</div>";
									
									$i++;
								}
								
								echo "</div>";

					}
				?>
			</div>
