			<div class="container">
			<div id="footermsg">
			<p>Tweety@AIFdb connects to the <a href="http://tweetyproject.org/w/delp/">Tweety@Web API</a> to evaluate <a href="http://aifdb.org">AIFdb</a> nodesets using Defeasible Logic Programming (DeLP). For details see the main reference: <i>Alejandro Garcia and Guillermo R. Simari. Defeasible Logic Programming: An Argumentative Approach. Theory and Practice of Logic Programming 4(1-2):95--138, 2004</i>.</p>
			
			<p>AIF graphs are translated into DeLP programs according to the following principles:
			<ul>
				<li>An application of a rule of inference in an AIF graph is a defeasible rule in DeLP
				<li>An application of a rule of conflict in an AIF graph is a defeasible rule in DeLP, where the consequent of the rule is the negate of the conflicted statement in the AIF graph
				<li>All non-conflicted premises in an AIF graph are facts in DeLP.
			</ul>
			
			</div>
			</div>
