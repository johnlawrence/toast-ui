<p>TOAST has a RESTful API, to allow you to use it in your own applications.</p>

<p>The base URL for the API is:</p>

	<blockquote>http://www.arg.dundee.ac.uk/toast/api/</blockquote>

<p>The following methods are available:</p>

<h2>POST evaluate</h2>

<p>Description: evaluates an argumentation theory, returning a JSON description</p>
<p class="apidesc">Parameters:</p>
<ul>
	<li>axioms - semi-colon delimited list of axioms</li>
	<li>premises - semi-colon delimited list of premises<li>
	<li>assumptions - semi-colon delimited list of assumptions</li>
	<li>kbPrefs - semi-colon delimited list of knowledge base preferences</li>
	<li>rules - semi-colon delimited list of rules</li>
	<li>rulePrefs - semi-colon delimited list of rule preferences</li>
	<li>contrariness - semi-colon delimited list of contrariness relations</li>
	<li>link - weakest or last-link preference principle</li>
	<li>semantics - grounded, preferred, semi-stable or stable semantics</li>
	<li>query - formula you wish to find an (un)acceptable argument for</li>
</ul>

<p>Note that the format of the above parameters is identical to the format used to enter them into the
web-based interface. Please consult the <a href="web">web help</a> for information.</p>

<h2>GET aifdb/&lt;argument&gt;</h2>

<p>Description: evaluates the provided argument ID in <a href="/AIFdb">AIFdb</a>, returning a JSON description.</p>


