<p>TOAST (The Online Argument Structures Tool) is a web-based implemented of the ASPIC+ framework, characterised by Prakken [2010]. Currently, it works only with a simple
propositional logic and simple formulae in the knowledge base 
(i.e. logical conectives aren't currently parsed). The system is a work-in-progress,
however, so new features will be added before long.</p>

<p>There are two interfaces to TOAST - a web form and a web service. The web form allows you to enter an argumentation system, view the results of evaluating it. The web service interface allows you to connect your own applications to TOAST and recieve the results in JSON format. Documentation for the web service is <a href="api">available</a>.</p>

<p>The syntax for using the TOAST web interface is as follows:</p>

<h2>Knowledge base</h2>

<p>The knowledge base is split into Axioms, Premises and Assumptions. For all
three sets, formulae are entered as a semi-colon seperated list of terms, 
with "~" being used for classical negation
e.g. p; q; something_in_prose; ~r;</p>

<p>Note that formulae cannot contains spaces.</p>

<h2>Knowledge base preferences</h2>

<p>The knowledge base prefereces are entered as a semi-colon seperated
list of elements, with each element having the following format:</p>

<p>
<b>lessPreferredFormula &lt; morePreferredFormula;</b>   
</p>

<p>
Note that as per the ASPIC+ theory, axioms cannot be part of a preference
relation.</p>

<h2>Rules</h2>
<p>Rules are entered in the following format:</p>

<p><b>[label] {comma-seperated list of antecedents}{implication}{consequent};</b></p>

<p>
Note: there must be a space between the label and the start of the rule.</p>

<p>
Antecedents and consequents are wffs in the language being used.
Implication is either "=>" (for a defeasible rule) or "->" for a strict
rule.</p>

<p>For instance:</p>

<p><b>[r1] p,q->r;</b> (a strict rule for r, with antecedents p and q, with label r1)<br />
<b>[r2] s=&gt;~t;</b> (a defeasible rule for ~t, with antecedent s, with label r2)</p>

<p>All rules MUST be labelled. Using a rule's label allows it to be undercut, by
expressing another rule with the negate of the rule's label as its
consequent, thus:</p>

<p><b>[r3] u=>~[r1];</b> (a defeasible rule where u undercuts the rule r1)
</p>
<p>
General rules are currently NOT supported, but including this is a high
priority.
</p>

<h2>Rule preferences</h2>

<p>Rule preferences are entered as a semi-colon seperated list of elements,
with each element having the following format:</p>
<p>
<b>[label_of_less_preferred_rule] &lt; [label_of_more_preferred_rule]</b>
<Br /><br />
e.g.<br /><br />
<b>[r1] &lt; [r2]</b>
</p>
<p>Note that as per the ASPIC+ theory, strict rules cannot be part of a 
preference relation.</p>

<h2>Contrariness</h2>
<p>
Contrariness relations allow one formula to be declared the contrary of
another, or for two formulae to be declared contradictory.</p>

<p>
TOAST accepts both these these forms. Examples will be used to describe
the syntax:</p>

<p>
<b>p^q;</b>
</p>

<p>This means that p is a contrary of q.</p>

<p>
<b>r-s;</b>
</p>

<p>
This means that r and s are contradictory, and is the same as:
</p>

<p>
<b>r^s;<br />s^r</b>;
</p>

<h2>Query</h2>

<p>The query determines whether or not there is an acceptable argument for the
given formula, under the specified semantics, in the abstract framework derived from the given AS.</p>

<h2>Last link/weakest link</h2>

<p>This decides whether the last link or weakest link principle is used to
determine the ordering on arguments. Please refer to [1] for details.</p>

<h2>References</h2>

<p>[1] H. Prakken. An abstract framework for argumentation using structured
arguments. <i>Argument and Computation</i>, 1(2):93-124, 2010.</p>
