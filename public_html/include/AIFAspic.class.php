<?php

class AIFAspic{

	private $json;
	private $nodes = array();
	private $premises = array();
	private $ra = array();
	private $ca = array();
	private $pa = array();
	private $l = array();
	private $ya = array();
	private $conclusions = array();
	private $rules = array();
	private $edges = array();
	private $contraries = array();
	
	private $locutions = array();
	
	private $rulePreferences = array();
	private $kbPreferences = array();
	private $undercutters = array();

	private $useText = false;

	function AIFAspic($json, $useText = false){

		$this->json = $json;
		$this->processNodes();
		$this->processEdges();	
		$this->useText = $useText;
	}

	function processNodes(){
		$edges = $this->json->edges;
	
		foreach($this->json->nodes as $node){
			if($node->type=="RA"){
				$this->ra[$node->nodeID] = $node;
			}else if($node->type=="CA"){
				$this->ca[$node->nodeID] = $node;
			}else if($node->type=="PA"){
				$this->pa[$node->nodeID] = $node;
			}else if($node->type=="L"){
				$this->l[$node->nodeID] = $node;
			}else if($node->type=="YA"){
				$this->ya[$node->nodeID] = $node;
			}
		}
		foreach($this->json->nodes as $node){			
			if($node->type=="I"){

				$node->text = str_replace(" ", "_", $node->text);
				$node->text = str_replace(",", "", $node->text);
				$node->text = str_replace("'", "", $node->text);

				$this->nodes[$node->nodeID] = $node;

				$premise = true;

				foreach($edges as $edge){
					if($edge->toID==$node->nodeID && array_key_exists($edge->fromID,$this->ra)){
						$premise = false;
						break;
					}	
				}

				if($premise)
					$this->premises[] = $node;			
			}
		}
		
		foreach($this->json->nodes as $node){
			if($node->type=="YA"){
			$scheme = $node->scheme;
				foreach($this->json->edges as $edge){
				//print_r($edge);
					if($edge->fromID==$node->nodeID){
						if(array_key_exists($edge->toID,$this->nodes)){
							$secondNode = $this->nodes[$edge->toID];
							if($secondNode->type=="I"){
								$this->locutions[] = array("scheme" => $scheme, "text" => $secondNode->text);
							}
						}
					}	
				}
			}
		}
	}

	function processEdges(){
		foreach($this->ra as $raNode){
			$tempRule = array("antecedents" => array(), consequent=> "");

			foreach($this->json->edges as $edge){

				if($edge->toID==$raNode->nodeID){
					if(!array_key_exists($edge->fromID,$this->nodes))
						continue;

					$tempRule["antecedents"][] = $edge->fromID; //str_replace(" ", "_", $this->nodes[$edge->fromID]->text);
					foreach($this->json->edges as $edge2){
						if($edge2->fromID==$raNode->nodeID){
							if(!array_key_exists($edge2->toID,$this->nodes))
								continue;
							$tempRule["consequent"] = $edge2->toID; //str_replace(" ", "_", $this->nodes[$edge2->toID]->text);
						}
					}
				}

				
				$this->rules[$raNode->nodeID] = $tempRule;
			}
		}

//		print_r($this->rules);

		//print_r($this->ca);

		foreach($this->ca as $caNode){
			$tempContrary = array();
			$contrary = "";
		
			foreach($this->json->edges as $edge){
				if($edge->toID==$caNode->nodeID){
					if(!array_key_exists($edge->fromID,$this->nodes))
						continue;
					$tempContrary[] = $edge->fromID; //str_replace(" ", "_", $this->nodes[$edge->fromID]->text);
					foreach($this->json->edges as $edge2){
						if($edge2->fromID==$caNode->nodeID){
							if(array_key_exists($edge2->toID, $this->rules)){
								$this->undercutters[$edge2->toID] = $edge->fromID;
							}
							$contrary = $edge2->toID; //str_replace(" ", "_", $this->nodes[$edge2->toID]->text);
						}
					}
				}
				
			}
			$this->contraries[$contrary] = $tempContrary;
		}

//		print_r($this->contraries);

		foreach($this->pa as $paNode){
			foreach($this->json->edges as $edge){
				if($edge->toID==$paNode->nodeID){
					$fromID = $edge->fromID;
					//the node that this is from is more preferred
					if(array_key_exists($fromID, $this->ra)){
						//rule preference
						$pref = array(0 => $fromID, 1 => -1);
						
						foreach($this->json->edges as $edge2){
							if($edge2->fromID==$paNode->nodeID){
								//the toID of this edge is the other end of the preference
								$pref[1] = $edge2->toID;
								break;
							}
						}
						
						if($pref[1]!=-1)
							$this->rulePreferences[] = $pref;
						
					}else if(array_key_exists($fromID, $this->nodes)){
						//premise preference
						$pref = array(0 => $fromID, 1 => -1);
						
						foreach($this->json->edges as $edge2){
							if($edge2->fromID==$paNode->nodeID){
								//the toID of this edge is the other end of the preference
								$pref[1] = $edge2->toID;
								break;
							}
						}
						if($pref[1]!=-1)
							$this->kbPreferences[] = $pref;
						
					}
				}
			}
		}


	}


	function getAspicArray($useArrays = false, $newline="\n"){
		
		$aspicArray = array("premises" => "",
					"rules" => "",
					"contrariness" => "",
					"rulePrefs" => "",
					"kbPrefs" => "");

		$thePremises = "";		
		
		foreach($this->premises as $premise){
			$i++;
			
			if($this->useText){
				$thePremises .= str_replace(" ","_",$premise->text) . ";$newline";
			}else{
				$thePremises .= $premise->nodeID . ";$newline"; //str_replace(" ", "_", $premise->text) . ";\n";
			}
		}
		
		/*if($useArrays){
			$aspicArray["premises"] = explode(";$newline",trim($thePremises," \t\n\r\0\x0B;"));
		}else{
			$aspicArray["premises"] = trim($thePremises);
		}*/

		$i = 1;	


		$raToRuleMapping = array();
		
		$theRules = "";

		foreach($this->rules as $ra => $rule){
		
			$ant = "";
		
			if($this->useText){
				foreach($rule["antecedents"] as $a){
					$ant .= $this->nodes[trim($a)]->text . ",";
				}
				$ant = trim($ant,",");
				$cons = $this->nodes[trim($rule["consequent"])]->text;
			
			}else{
				$ant = implode(",", $rule["antecedents"]);
				$cons = $rule["consequent"];
			}



			$theRules .= "[r$i] $ant=>$cons;$newline";
			$raToRuleMapping[$ra] = "r$i";
			$i++;
			
			
		}

		foreach($this->undercutters as $rule=>$undercutter){
			$ruleID = $raToRuleMapping[$rule];
			$theRules .= "[r$i] $undercutter=>~[$ruleID];$newline";
			$i++;
		}
		
		/*if($useArrays){
			$aspicArray["rules"] = explode(";$newline",trim($theRules));
		}else{
			$aspicArray["rules"] = $theRules;
		}*/

		$contraries = array_keys($this->contraries);

		$cont = "";
		
		foreach($this->contraries as $contradicted => $contraries){
			$contradictedText = $this->nodes[$contradicted]->text;
			$text = $contradictedNode->text;
			foreach($contraries as $c){
			
				if($this->useText){
					$contText = $this->nodes[$c]->text;
					$cont .= "$contText^$contradictedText;$newline";
				}else{
					$cont .= "$c^$contradicted;$newline";
				}
			}
		}

		/*foreach($this->contraries as $cont){
			foreach($this->contraries[$cont] as $cont2){
			
				if($this->useText){
					$node = $this->nodes[$cont];
					$cont = $node->text;
					
					$node = $this->nodes[$cont2];
					$cont2 = $node->text;
				}
			
				$cont .= "$cont2^$cont;$newline";
			}
		}*/
		
		/*if($useArrays){
			$aspicArray["contrariness"] = explode(";$newLine",trim($cont));
		}else{
			$aspicArray["contrariness"] = $cont;
		}*/
		
		$rulePrefs = "";
		$kbPrefs = "";
		
		foreach($this->rulePreferences as $pref){
			$rulePrefs .= $raToRuleMapping[$pref[1]] . " < " . $raToRuleMapping[$pref[0]] . ";$newline";
		}
		
		foreach($this->kbPreferences as $pref){
			$kbPrefs .= $pref[1] . " < " . $pref[0] . ";$newline";
		}
		
		if($useArrays){
			$charMask = " \t\n\r\0\x0B;";
			$aspicArray["premises"] = explode(";$newline",trim($thePremises, $charMask));
			$aspicArray["rules"] = explode(";$newline",trim($theRules, $charMask));
			$aspicArray["contrariness"] = explode(";$newline",trim($cont, $charMask));
			$aspicArray["rulePrefs"] = explode(";$newline",trim($rulePrefs, $charMask));
			$aspicArray["kbPrefs"] = explode(";$newline",trim($kbPrefs, $charMask));
			//if(isset($_GET['locutions']))
				$aspicArray["locutions"] = $this->locutions;
		}else{
			$aspicArray["premises"] = trim($thePremises);
			$aspicArray["rules"] = trim($theRules);
			$aspicArray["contrariness"] = trim($cont);
			$aspicArray["rulePrefs"] = trim($rulePrefs);
			$aspicArray["kbPrefs"] = trim($kbPrefs);
			$aspicArray["locutions"] = $this->locutions;
		}
	
		return $aspicArray;

	}


}


?>
