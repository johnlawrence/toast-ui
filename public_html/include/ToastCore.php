<?php
  require_once "vendor/autoload.php";
	include "include/AIFAspic.class.php";

class ToastDB extends SQLite3 {
    function __construct() {
        $this->open('/var/www/html/toast.db');
    }
}

	if(isset($_GET['nodeset'])){

       	$ns = $_GET['nodeset'];

        if(is_numeric($ns)){
        	$url = "http://aifdb.org/json/$ns";
        } /*else{
        	$url = "http://corpora.aifdb.org/json/$ns";
        }*/

		$json_text = file_get_contents($url);

		if(substr($json_text,0,1)!='{'){
			$json_text = substr($json_text,6,strlen($json_text));
		}

        $json = json_decode($json_text);

        if($json==null)
        	return;

	    $aifAspic = new AIFAspic($json);

 	    $arr = $aifAspic->getAspicArray();
        $keys = array_keys($arr);

	    foreach($keys as $key){
        	eval('$' . $key . ' = "' . $arr[$key] . '";');
	    }
	}else if(isset($_GET['load'])){

		$id = $_GET['load'];

		if(intval($id) != 0){
			$db = new ToastDB();
			$result = $db->query("SELECT * FROM arguments WHERE argID=$id;");
			if($result){
					while($row = $result->fetchArray(SQLITE3_ASSOC)){
						$data = str_replace('{', '"', base64_decode($row["data"]));
						$data = str_replace('}', '"', $data);
						eval('$data=' . $data);
						foreach($data as $key => $value){
							eval('$' . $key . '="' . $value  . '";');
						}
					}
			}
		}else{
			$db = (new MongoDB\Client('mongodb://arg-tech-toast-mongo'))->toast;
			$toastDB = $db->arguments;
			$result = $toastDB->findOne(Array("id" => $_GET['load']));

			if($result){
				$result = json_decode(MongoDB\BSON\toJSON(MongoDB\BSON\fromPHP($result)));
				foreach($result as $key=>$value){
					if($key == "_id") continue;
					if(is_array($value)){
						$value = implode(";", $value) . ";";
					}
					eval('$' . $key . '="' . $value . '";');
				}
			}
		}
	}else{
		$axioms = $_POST['axioms'];
		$assumptions = $_POST['assumptions'];
		$premises = $_POST['premises'];
		$rules = $_POST['rules'];
		$kbPrefs = $_POST['kbprefs'];
		$rulePrefs = $_POST['ruleprefs'];
		$query = $_POST['query'];
		$link = $_POST['link'];
		$contrariness = $_POST['contrariness'];
		$language = $_POST['language'];
		$semantics = $_POST['semantics'];
	}
?>
