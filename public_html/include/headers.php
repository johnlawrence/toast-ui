<?php include "ToastCore.php";

define('REQUEST_URL',$_SERVER['HTTP_HOST']);

?><!DOCTYPE html>
<html>
        <head>

                <meta charset="UTF-8" />
                <title>TOAST from ARG-tech</title>
                <script type="text/javascript" src="/include/scripts/jquery.js"></script>
                <script type="text/javascript" src="/include/scripts/jquery.form.js"></script>
                <script type="text/javascript" src="/include/scripts/jquery.tooltip.js"></script>
                <script type="text/javascript" src="/include/scripts/toast.core.min.js"></script>
		<script type="text/javascript">

            $(document).ready(function(){
                $("script,img").each(function(){
                    $(this).attr("src", $(this).attr("src") + "?test=true");
                });

                $("link,a").each(function(){
                    $(this).attr("href", $(this).attr("href") + "?test=true");
                });

                $("#aspicForm").attr("action",$("#aspicForm").attr("action") + "?test=true");

                var odd = true;

                $("table.api tr").each(function(){
                    if(odd){
                        $(this).css("background-color","#FFFFFF");
                        $(this).find("td").css("border-left","1px solid  #EFECE8");
                    }else{

                    }
                    odd=!odd;
                });
            });



			  var _gaq = _gaq || [];
			  _gaq.push(['_setAccount', 'UA-10861232-3']);
			  _gaq.push(['_trackPageview']);

			  (function() {
			    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			  })();

			</script>
                <link rel="stylesheet" href="/include/style/style.css" />
        </head>
        <body>
        <div id="hdr" style="background-color:black; height:70px;">
            <div id="hdr_content">
                <a href="/"><img src="/include/img/toast-logo.png" alt="TOAST" style="height:50px;padding-top:2px;"/></a>
                <a href="http://arg.tech"><img src="/include/img/arg-tech.png" style="height:30px; float:right; position:relative; top:20px;"/></a><br />
                <span id="hdr_txt">The Online Argument Structures Tool</span>

            </div>
        </div>
                <div id="ccol">
                        <header style="position:relative; top:-30px;">
                                <!--<a href="/"><img src="/include/img/toast-logo.png" alt="TOAST" style="height:50px;"/></a>-->
                                        <!--<span id="header-subtxt">The Online Argument Structures Tool</span><br />-->
                                        <span id="header-TXT"><!--from <a href="http://arg-tech.org">ARG-tech</a>-->
                                        <!--<img src="/include/img/arg-tech.png" style="height:30px; position:relative; top:-22px;"/>--></span>
                        </header>
