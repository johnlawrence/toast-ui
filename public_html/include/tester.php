<?php

if($_GET['pass']!="marksnaith")
	die("You shouldn't be here...");

include "AIFAspic.class.php";

$json = array("nodes" => 
			array(
				array("type" => "I",
						"nodeID" => 1,
						"text" => "test node"),
				array("type" => "I",
						"nodeID" => 2,
						"text" => "another test node"),
				array("type" => "PA",
						"nodeID" => 3,
						"text" => "PA"),
				array("type" => "RA",
						"nodeID" => 4,
						"text" => "RA"),
				array("type" => "RA",
						"nodeID" => 5,
						"text" => "RA")
					),
				"edges" =>
					array(
						array("edgeID" => 1,
								"fromID" => 4,
								"toID" => 3),
						array("edgeID" => 2,
								"fromID" => 3,
								"toID" => 5),
						array("edgeID" => 4,
								"fromID" => 1,
								"toID" => 4),
						array("edgeID" => 5,
								"fromID" => 4,
								"toID" => 2),
						array("edgeID" => 6,
								"fromID" => 2,
								"toID" => 5),
						array("edgeID" => 7,
								"fromID" => 5,
								"toID" => 1)
						)
					);
					
$j = json_encode($json);
//print_r(json_decode($j));

$aif = new AIFAspic(json_decode($j));

print_r($aif->getAspicArray());



?>