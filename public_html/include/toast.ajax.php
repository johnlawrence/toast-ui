<?php

require_once "../vendor/autoload.php";




// class ToastDB extends SQLite3 {
//     function __construct() {
//         $this->open('/var/www/html/toast.db');
//     }
// }
//
// $db = new ToastDB();
// if(!$db){
//     echo $db->lastErrorMsg();
// }else{
//     $sql = <<< EOT
//                 CREATE TABLE arguments
//                 (argID INTEGER PRIMARY KEY AUTOINCREMENT,
//                  data TEXT NOT NULL);
// EOT;
//     $ret = @$db->exec($sql);
// }


error_reporting(1);
ini_set('display_errors', 'on');
session_start();
$id = -1;;
$database_id = -1;

if($_SERVER['SERVER_PORT'] != 80){
  $port = ":".$_SERVER['SERVER_PORT'];
}else{
  $port = "";
}

$host = "http://" . $_SERVER['SERVER_NAME'] . $port;



if($_GET['example']=="true"){
	echo <<< EOT
		{"premises":"snores(bob); professor(bob);",
		"kbPrefs":"snores(X) < professor(X);",
		"rules":"[r1] snores(X) => misbehaves(X); [r2] misbehaves(X) => accessDenied(X); [r3] professor(X) => accessAllowed(X);",
		"rulePrefs":"[r1] < [r2]; [r1] < [r3]; [r3] < [r2];",
		"contrariness":"accessDenied(X)-accessAllowed(X);",
		"axioms":"",
		"assumptions":"",
		"semantics":"grounded",
		"query":"accessDenied(bob)"}
EOT;

}

else{
	$result = getResult();
	echo getResults($result);
}

function getResults($result){

	global $database_id,$host;

	global $id;
	$toReturn = "";

	$showIslands = ($_POST['islands']=="true") ? true : false;

	$jsonText = json_encode($result);
	$nodeset = $_GET['nodeset'];

	$acceptableJS = "";

	foreach($result->acceptableConclusions as $id => $acceptable){
		$acceptableJS .= " acceptableConclusions[$id] = ['" . implode("','",$acceptable) . "'];";
	}




	echo <<< EOT
		<script type="text/javascript">

			var acceptableConclusions = new Array();

			{$acceptableJS}

			var whichFramework = 'abstract';
			var imageCache;

			$(document).ready(function(){
				imageCache = new Object();
				imageCache['abstract'] = new Object();
				imageCache['structured'] = new Object();
				getImage(1);
			});

			function showHide(which, sender){
				if($("#" + which).css("display")=="none"){
					$("#" + which).slideDown();
					$(sender).html("Hide");
				}else{
					$("#" + which).slideUp();
					$(sender).html("Show");
				}

				return false;
			}

			function checkImageLoaded(img){
				if(!img.complete){
					setTimeout(checkImageLoaded, 10, img);
				}
			}

			function getImage(ext){

				var img = new Image();

				$(".arglink").each(function(){
					if($.inArray($(this).attr("id"),acceptableConclusions[ext-1])==-1){
						$(this).css("color","red");
					}else{
						$(this).css("color","green");
					}
				});

				$(img).on('load',function(){
						setTimeout(checkImageLoaded, 10, img);
						$("#img_ext").attr("src",img.src);

						var w;

						if(img.width > 700){
							w = 700;
						}else{
							w = img.width;
						}

						$("#img_ext").css("width", w + "px").css("height","100%");

						$("#img_ext_link").attr("href",img.src);

					});

				if(typeof imageCache[whichFramework][ext]!='undefined'){
					img.src = imageCache[whichFramework][ext];
				}else{
					$("#img_ext").attr('src','/include/img/loader-larger.gif').css('height','100px').css('width','100px');

					var url = "/include/img/getImage.php?type=" + whichFramework + "&islands={$showIslands}&extension=" + ext;

					if(whichFramework=='structured'){
						url += '&nodeset={$nodeset}';
					}

					$.ajax({
						type: "POST",
						url: url,
						data: {$jsonText},
						success: function(response){
                            img.src = imageCache[whichFramework][ext] = 'data:image/png;base64,' + response;

						}
					});
				}
			}

			function showFramework(which){
				whichFramework = which;
				getImage(1);
				return false;
			}
		</script>
EOT;

	if(!$result->wellformed){
		$toReturn .= "<b>WARNING: the argumentation theory is not well-formed (<a onclick=\"return false;\" href=\"\" class=\"wellFormedWhy\" alt=\"" . $result->wellFormedReason . "\">why?</a>)</b><br /><br />";
	}

    if($result->messages!=null){
        $toReturn .= "<b>" . $result->messages[0] . "</b><br /><br />";
    }



	$aifdb = ($_POST['aifdb']=="true") ? true : false;

	$toReturn .= "<div id=\"frameworkContainer\">";

	$toReturn .= "<div class=\"container_topbar\" id=\"arguments_container_topbar\">Arguments (<a href=\"\" onclick=\"return showHide('arguments_container',this);\">Hide</a>)</div><br /><div id=\"arguments_container\"><div id=\"arguments\" style=\"display:block;\"><ul>";

	$args = $result->arguments;
	natsort($args);

	$extensions = !empty($result->extensions);

	$tooltips = array();

	foreach($args as $arg){
        $re = '/([A-Z0-9]+): ([A-Z0-9, ]+)*(->|=>)?(.*)/m';
        preg_match_all($re, $arg, $matches, PREG_SET_ORDER, 0);

        $argParts = $matches[0];

        $label = $argParts[1];
        $subArgs = trim($argParts[2]);
        $inference = $argParts[3];
        $argId = $argParts[4];

		$link = ($aifdb) ? " href=\"http://www.arg.dundee.ac.uk/AIFdb/nodeview/$argId\" target=\"_new\"" : "";

		$newArg = $label . ": " . trim($subArgs) . trim($inference) . "<a id=\"$argId\"$link class=\"arglink\" style=\"cursor:hand;\" alt=\"$argId\">" . $argId . "</a>";

		$toReturn .= "<li>" . $newArg . "</li>";
	}


	$toReturn .= "";

	$img1 = $images[0]["image"];
	$ext1 = $images[0]["extension"];

	$tempExt = array();

	foreach($ext1 as $arg){
		$exploded = explode(":",$arg);
		$tempExt[] = trim($exploded[0]);
	}

	$first = true;

	$toggleLinks = array();
	$i = 1;

	$ext = ($extensions) ? "{" . implode(", ",$ext) . "}" : "<br />There are no extensions";

	$currentExt = 0;

	while(property_exists($result->extensions, $currentExt)){
		$currentExt++;
		$toggleLinks[] = "<a href=\"javascript:getImage($currentExt); void(0);\">$currentExt</a>";
	}

	$toggle = "";
	$diagram = "";

	if($aifdb){
		$diagram = "<br /><br />Show diagram: <a href=\"\" onclick=\"return showFramework('abstract');\">Abstract framework</a> | <a href=\"\" onclick=\"return showFramework('structured');\">Structured framework</a><br /><br />";
	}


	if($extensions){
		$toggle = "<br /><br />Show extension: " . implode(" ",$toggleLinks);
	}else{
		$toggle = $ext;
	}

	$toReturn .= "</ul></div></div><br /><br /><div class=\"container_topbar\" id=\"img_container_topbar\">Extensions (<a href=\"\" onclick=\"return showHide('container_img',this);\">Hide</a>)</div><div id=\"container_img\"><br /><div id=\"img\"><a id=\"img_ext_link\"><img id=\"img_ext\" src=\"/include/img/loader-larger.gif\" style=\"display:block; margin-left:auto; margin-right:auto; height:100px;\" /></a>$toggle $diagram</div></div><br /><br />";

	$toReturn .= "</div>";
	$toReturn .= "<span style=\"font-size:12pt;\">Share by using this URL: <a href=\"$host/$database_id\" target=\"_new\">$host/$database_id</a></span>";
	return $toReturn;
}


function getImages($args, $def, $ext, $showIslands=true){
	asort($ext);

	$_SESSION['img'] = array();


	$i = 0;
	$images = array();

	if(empty($ext))
		$ext = array(array());

	$base_dir = "/var/www/html/temp";

	foreach($ext as $extension){
		$i++;
		$dot = createDot($args,$def,$extension,$showIslands);
		$filename = md5(time("d/m/y") . $i);

		$myfile = "/home/toast/public_html/temp/$filename.dot";
		$fh = fopen($myfile, 'w');
		fwrite($fh, $dot);
		fclose($fh);

		echo "dot -Tpng $myfile -o $base_dir/$filename.png";

		$out = exec("dot -Tpng $myfile -o $base_dir/$filename.png",$output);
		$img = file_get_contents("$base_dir/$filename.png");
		exec("rm $base_dir/$filename.png");

		unset($_SESSION['img'][$i]);
		$_SESSION['img'][$i] = $img;

		exec("rm $base_dir/$filename.dot");
		$images[] = array("image" => $i,
					"extension" => $extension);
	}

	return $images;
}

function processProgram($input){
    $regex = array("kb" => '([aps]\..*)',
               "rules" => '(\[.*\][ ]+[^< ]+)',
               "kbPrefs" => '([^\[\] ][ ]*<[ ]*[^\[\] ])',
               "rulePrefs" => '(\[.*\][ ]+<[ ]+\[.*\])',
               "contrariness" => '(.*[\^|-].*)');

    $kb = array();
    $rules = array();
    $kbPrefs = array();
    $rulePrefs = array();
    $contrariness = array();

    foreach(explode(";",$input) as $line){
        $line = trim($line);
        foreach($regex as $type => $re){
            $re = '/' . $re . '/m';
            preg_match_all($re, $line, $matches, PREG_SET_ORDER, 0);

            if(sizeof($matches) > 0){
                echo "$line is a $type<br />";
                eval('$' . $type . '[]="' . trim($line) . '";');
            }
        }
    }

    $program = array("axioms" => array(),
                     "premises" => array(),
                     "assumptions" => array(),
                     "rules" => $rules,
                     "kbPrefs" => $kbPrefs,
                     "rulePrefs" => $rulePrefs,
                     "contrariness" => $contrariness);


    foreach($kb as $k){
        if(substr($k,0,1)=='a'){
            $program["axioms"][] = substr($k, 2);
        }elseif(substr($k,0,1)=='p'){
            $program["premises"][] = substr($k, 2);
        }if(substr($k,0,1)=='s'){
            $program["assumptions"][] = substr($k, 2);
        }
    }

    return $program;

}



function getResult(){
	global $id;
	global $database_id;

    $components = array("axioms","premises","assumptions","rules","kbPrefs","rulePrefs","contrariness");

    // foreach($components as $component){
    //   echo $component;
    //   $c = $_POST[$component];
    //
    //   if (substr($string, -1) != ';') $c .= ";";
    //
    //   eval('$' . $component . '=$c;');
    // }



    if(isset($_POST['program'])){
        $arr = processProgram($_POST['program']);

        $axioms = $arr["axioms"];
        $assumptions = $arr["assumptions"];
        $premises = $arr["premises"];
        $rules = $arr["rules"];
        $kbPrefs = $arr["kbPrefs"];
        $rulePrefs = $arr["rulePrefs"];
        $contrariness = $arr["contrariness"];
    }else{
      foreach($components as $component){
        $c = trim($_POST[$component]);
        if ($c !="" && substr($c, -1) != ';') $c .= ";";
        eval('$' . $component . '=$c;');
      }
    }

    $query = $_POST['query'];
    $link = $_POST['link'];
    $language = $_POST['language'];
    $semantics = $_POST['semantics'];
    $transposition = $_POST['transposition'];


	$engine = $_POST['engine'];

	$output = <<< EOT

	array("axioms" => "{$axioms}",
		"assumptions" => "{$assumptions}",
		"premises" => "{$premises}",
		"rules" => "{$rules}",
		"kbPrefs" => "{$kbPrefs}",
		"rulePrefs" => "{$rulePrefs}",
		"query" => "{$query}",
		"link" => "{$link}",
		"contrariness" => "{$contrariness}",
		"language" => "{$language}",
		"semantics" => "{$semantics}",
		"transposition" => "{$transposition}");
EOT;

//
//
//
//
//     $data = base64_encode($output);
//
//     $output = addslashes($output);
//     $database_id = -1;
//
//     exec('$d=' . $output);
//     print_r($d);
//
//     // $db = new ToastDB();
//     // $ret = $db->query("SELECT argID FROM arguments WHERE data='$data';");
//     //
//     // if($ret){
//     //     while($row = $ret->fetchArray(SQLITE3_ASSOC)){
//     //         $database_id = $row["argID"];
//     //     }
//     // }
//
// //     if($database_id==-1){
// //         $ret = $db->exec("INSERT INTO arguments(data) VALUES('$data');");
// // echo        $database_id = $db->lastInsertRowID();
// //
// //
// //         $ret = $db->query("SELECT argID FROM arguments WHERE data='$data';");
// //
// //         echo $db->lastErrorMsg();
// //
// // 	print_r($ret);
// //
// //         if($ret){
// //             while($row = $ret->fetchArray(SQLITE3_ASSOC)){
// //                 $database_id = $row["argID"];
// //             }
// //         }
// //
// //     }

	$fields = array("axioms" => $axioms,
		"assumptions" => $assumptions,
		"premises" => $premises,
		"rules" => $rules,
		"kbPrefs" => $kbPrefs,
		"rulePrefs" => $rulePrefs,
		"query" => $query,
		"link" => $link,
		"contrariness" => $contrariness,
		"language" => $language,
		"semantics" => $semantics,
		"transposition" => $transposition);


  $db = (new MongoDB\Client('mongodb://arg-tech-toast-mongo'))->toast;

  $toastDB = $db->arguments;
  $counters = $db->counters;



	$data = "";

	$json_data = array();

	foreach($fields as $field => $value){
		if(strpos($value, ";")!==false){
			$json_data[$field] = explode(";",rtrim($value,";"));
    }else{
			$json_data[$field] = $value;
    }
	}

  $json = json_encode($json_data);
  $hash = md5($json);
  $json_data["hash"] = $hash;

  $result = $toastDB->findOne(Array("hash" => $hash));

  if($result){
      $database_id = $result["id"];
  }else{
      $database_id = $json_data["id"] = get_unique_id();
      $toastDB->insertOne($json_data);
  }

	$host = getenv("TOAST_URL");

	if(!$host){
	    $host = "arg-tech-toast";
	}

	$port = getenv("TOAST_PORT");

	if(!$port){
	    $port = 1234;
	}

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "http://$host:$port/evaluate?general=true");
	curl_setopt($ch, CURLOPT_POST, count($json_data));
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_data));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$response = curl_exec($ch);

	$theResponse = json_decode($response);

	return $theResponse;
}

function createDot($args,$def,$ext,$showIslands=true){

	$acceptableArgs = array();

	foreach($ext as $arg){
		$argAndLabel = explode(":",$arg);
		$acceptableArgs[] = trim($argAndLabel[0]);
	}

//print_r($def);


	$argLabels = array();
	foreach($args as $arg){
		$split = explode(":",$arg);
		$argLabels[] = trim($split[0]);
	}


	$islands = array();

	$inDef = false;
	$currentLabel = "";

	for($i=0;$i<sizeof($argLabels);$i++){
		$inDef = true;
		for($j=0;$j<sizeof($def);$j++){
			if(strpos($def[$j],$argLabels[$i])===false){
				$inDef = false;
			}
		}
		if(!$inDef)
			$islands[] = $argLabels[$i];
	}



	$theDot = "digraph framework {\n bgcolor = \"#EEEEEE\";\n";

	$proceed = true;

	foreach($argLabels as $arg){

		$proceed = true;

		if($showIslands){
			if($proceed){
				$theDot .= $arg;
				if(in_array($arg,$acceptableArgs)){
					$theDot .= "[style=filled,color=\"#008C00\"]";
				}else{
					$theDot .= "[style=filled,color=\"#B02B2C\"]";
				}
				$theDot .= "\n";
			}
		}
		$theDot .= "rankdir=\"LR\";\n";
	}

	for($i=0;$i<sizeof($def);$i++){
		$split = explode(">",$def[$i]);
		if(!$showIslands){

			$theDot .= $split[0];

			if(in_array($split[0],$acceptableArgs)){
				$theDot .= "[style=filled,color=\"#008C00\"]";
			}else{
				$theDot .= "[style=filled,color=\"#B02B2C\"]";
			}
			$theDot .= "\n" . $split[1];

			if(in_array($split[1],$acceptableArgs)){
				$theDot .= "[style=filled,color=\"#008C00\"]";
			}else{
				$theDot .= "[style=filled,color=\"#B02B2C\"]";
			}
		}

		$theDot .= $split[0] . "->" . $split[1];

		if($i != sizeof($def)-1)
			$theDot .= ",";

		$theDot .= "\n";
	}

	$theDot .= "}";

//	echo $theDot;

	return $theDot;
}

function get_unique_id(){

  $alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890abcdefghijklmnopqrstuvwxyz";
  $len = strlen($alphabet) - 1;

  $newID = rand(0, 9);

  for($i=0;$i<5;$i++){
    $rand = rand(0, $len);
    $newID .= $alphabet[$rand];
  }

  $db = (new MongoDB\Client('mongodb://arg-tech-toast-mongo'))->toast;
  $toastDB = $db->arguments;

  $result = $toastDB->findOne(Array("id" => $newID));

  if($result){
    $id = $result["id"];

    if($id == $newID){
      return get_unique_id();
    }else{
      return $newID;
    }
  }else{
    return $newID;
  }
}
?>
