<?php $nodeset = (isset($_GET['nodeset'])) ? "nodeset=" . $_GET['nodeset'] . "&rand=" .  rand(0,getRandMax()) : "rand=" . rand(0,getRandMax());	?>

<!--<div style="background-color:#e2e2e4;display:block; padding:20px; font-size:10pt;text-align:justify;">
    <h1 style="font-size:14pt;">About</h1>
    <br />The Online Argument Structures Tool (TOAST) is an implementation of the ASPIC+ structured argumentation framework.
</div>
<br />-->
<div class="info">
    <h1>About</h1>
    The Online Argument Structures Tool (TOAST) is an implementation of the ASPIC+ theory of structured argumentation. Using the text fields below,
    build an argumentation theory consisting of a knowledge base, rules, contrariness and preferences. Evaluating the theory first translates it into a Dung-style
    abstract argumentation framework, which is subsequently evaluated for to determine the acceptability of the arguments. More information about the format of the
    knowledge base and rules can be found on the <a href="/help/web">help page</a>. A <a href="/help/api">RESTful API</a> is also available.
</div>

<div class="info" id="at">
 <h1>Argumentation Theory</h1>

<div class="container-outer">
    <form method="POST" action="/include/toast.ajax.php?<?php echo $nodeset; ?>" id="aspicForm">
    <input type="hidden" id="aifdb" name="aifdb" value="<?php echo (isset($_GET['nodeset'])) ? "true" : "false"; ?>" />

    <div class="container" id="program_outer">
        <!--<div class="container-row">-->
            <div id="axioms" class="formInput">
                Axioms:<br /><textarea id="txtAxioms" name="axioms" rows="10" cols="25"><?php echo stripslashes($axioms); ?></textarea>
            </div>
            <div id="premises" class="formInput">
                Premises:<br /><textarea id="txtPremises" name="premises" rows="10" cols="25"><?php echo stripslashes($premises); ?></textarea>
            </div>
            <div id="assumptions" class="formInput">
                Assumptions:<br /><textarea id="txtAssumptions" name="assumptions" rows="10" cols="25"><?php echo stripslashes($assumptions); ?></textarea>
            </div>
            <div id="kbPrefs" class="formInput">
                Preferences:<br /><textarea id="txtKbPrefs" name="kbPrefs" rows="10" cols="25"><?php echo stripslashes($kbPrefs); ?></textarea>
            </div>
        <!--</div>-->

        <!--<div class="container-row">	-->
            <div id="rules" class="formInput">
                Rules:<br /><textarea id="txtRules" name="rules" rows="10" cols="25"><?php echo stripslashes($rules); ?></textarea><br />
                <span style="font-size:9pt;"><a href="#" onclick="return addRuleLabels();">Add rule labels</a> | <input type="checkbox" name="transposition" id="transposition" value="yes" <?php if($transposition=="yes") echo "checked"; ?> /> Close under transposition?</span><br /><br />
            </div>
            <div id="rulePrefs" class="formInput">
                Rule Preferences:<br /><textarea id="txtRulePrefs" name="rulePrefs" rows="10" cols="25"><?php echo stripslashes($rulePrefs); ?></textarea>
            </div>
            <div id="contrariness" class="formInput">
                Contrariness:<br /><textarea id="txtContrariness" name="contrariness" rows="10" cols="25"><?php echo $contrariness; ?></textarea>
            </div>
            <div id="oth" class="formInput">
                Options: <br /><br />Preference principle: <br />
                <input type="radio" id="chkLast" name="link" value="last" <?php if($link=="last" || !isset($link)) echo "checked"; ?> /> Last link
                <input type="radio" id="chkWeakest" name="link" value="weakest" <?php if($link=="weakest") echo "checked"; ?>/> Weakest link<br /><br />
                <div style="display:inline-block;">
                    Evaluation engine:<br />
                    <select name="engine">
                        <option id="dom" value="dom">Dung-O-Matic</option>
                    </select>
                </div>
                <div style="display:inline-block; padding-left:20px;">
                    Semantics:<br />
                    <select name="semantics">
                        <option id="optGrounded" value="GROUNDED" <?php if($semantics=="GROUNDED" || !isset($semantics)) echo "SELECTED"; ?>>Grounded</option>
                        <option id="optPreferred" value="PREFERRED" <?php if($semantics=="PREFERRED") echo "SELECTED"; ?>>Preferred</option>
                        <option id="optSemiStable" value="SEMISTABLE" <?php if($semantics=="SEMISTABLE") echo "SELECTED"; ?>>Semi-Stable</option>
                        <option id="optStable" value="STABLE" <?php if($semantics=="STABLE") echo "SELECTED"; ?>>Stable</option>
                    </select>
                </div>
                <br /><br /><input type="checkbox" name="islands" value="true" CHECKED /> Show islands in abstract framework?
            </div>
        <!--</div>

        <div class="container-row" style="padding-top:10px;">-->
            <div id="query" style="width:100%;">
                Query: <input id="txtQuery" type="text" name="query" value="<? echo stripslashes($query); ?>" />
                    <input type="submit" value="Evaluate" id="process" onclick="javascript:addRuleLabels(true);" />
                    <a href="#" onclick="return loadExample();">Load an example</a> |
                    <a href="/help/web">Help</a> |
                    <a href="/help/api">API</a> |
                    <!--<a href="#" onclick="toggleAdvanced(this);">Advanced editor</a>-->
            </div>
        <!--</div>-->
    </div>
    </form>
</div>
</div>
<div class="info">
    <h1>Citation</h1>
    If you find TOAST useful, and wish to reference it, please use the following citation:<br /><br />Snaith, M. & Reed, C. (2012) "TOAST: online ASPIC+ implementation" in Verheij, B., Szeider, S. & Woltran, S. (eds.) <i>Proceedings of the Fourth International Conference on Computational Models of Argument (COMMA 2012)</i>, IOS Press, Vienna, pp509--510. [<a href="/snaith-reed-comma12.pdf">pdf</a>]
</div>
<div class="info">
    <h1>Source code</h1>
    Coming soon.
</div>
