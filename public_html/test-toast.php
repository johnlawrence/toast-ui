<?php

$regex = array("kb" => '([aps]\..*)',
               "rules" => '(\[.*\][ ]+[^< ]+)',
               "kbPrefs" => '([^\[\] ][ ]*<[ ]*[^\[\] ])',
               "rulePrefs" => '(\[.*\][ ]+<[ ]+\[.*\])',
               "contrariness" => '(.*[\^|-].*)');

$kb = array();
$rules = array();
$kbPrefs = array();
$rulePrefs = array();
$contrariness = array();


$program = <<< EOT

a.f(X);
p.g(X);
s.h(X);

[r1]          f(X) =>        d(X);
[r2] d(X) => t(X);

[r1]     <      [r2];

g(X) < f(X);
h(X)-f(X);
g(X)^f(X);
[r3] p => q;
EOT;

$p = explode(";", $program);

foreach($p as $line){

    foreach($regex as $type => $re){
        $re = '/' . $re . '/m';
        preg_match_all($re, $line, $matches, PREG_SET_ORDER, 0);
        
        if(sizeof($matches) > 0){
            echo "$line is a $type<br />";
            eval('$' . $type . '[]="' . trim($line) . '";');
        }
    }
}

print_r($kb);


?>