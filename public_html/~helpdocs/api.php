<?php $base_url = "http://" . REQUEST_URL . "/api"; ?>
<div class="info">
    <h1>TOAST API user guide</h1>
    
    <p>TOAST has a RESTful API, to allow you to use it in your own applications.</p>
    
    <p>
        The base URL for the API is:
        <span class="example">
	        <?php echo $base_url; ?>
	    </span>

        The following methods are available:
    </p>
	
			<h2>POST /evaluate</h2>
	
			<p>Description: evaluates an argumentation theory, returning a JSON description</p>
			
			<table class="api">
                <tr style="font-weight:bold;">
                    <td style="width:200px;">Name</td><td>Description</td>
                </tr>
                <tr>
                    <td>axioms</td><td>List of axioms</td>
                </tr>
                <tr>
                    <td>premises</td><td>List of premises</td>
                </tr>
                <tr>
                    <td>assumptions</td><td>List of assumptions</td>
                </tr>
                <tr>
                    <td>kbPrefs</td><td>List of knowledge base preferences (note: cannot contain any axioms)</td>
                </tr>
                <tr>
                    <td>rules</td><td>List of rules</td>
                </tr>
                <tr>
                    <td>rulePrefs</td><td>List of rule preferences</td>
                </tr>
                <tr>
                    <td>contrariness</td><td>List of contraries and contradictories</td>
                </tr>
                <tr>
                    <td>link</td><td>Weakest (default) or last link preference principle</td>
                </tr>
                <tr>
                    <td>semantics</td><td>Grounded (default), preferred or semi-stable</td>
                </tr>
                <tr>
                    <td>query</td><td>Formula to find an (un)acceptable argument for</td>
                </tr>
                <tr>
                    <td>transposition</td><td>True or false (default); whather or not to close the theory under transposition</td>
                </tr>
			</table>
			
			<p>Data can be posted either as key-value pairs (in which case all lists are delimited by semi-colons), or as a JSON object (in which case, all lists are JSON arrays). For instance, using cURL:
			<span class="example" style="width:650px;">
				curl -d "premises=p;q;&rules=[r1] p=&gt;s;&contrariness=q^p;"  <?php echo $base_url; ?>/evaluate
			</span>
		
		    Returns:
		
		    <span class="example">
				{<br />&nbsp;&nbsp;&nbsp;"wellformed":true,<br />&nbsp;&nbsp;&nbsp;"result":"",<br />&nbsp;&nbsp;&nbsp;"extensions":[<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"A1:&nbsp;q"<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]<br />&nbsp;&nbsp;&nbsp;],<br />&nbsp;&nbsp;&nbsp;"arguments":[<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"A3:&nbsp;A2=>s",<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"A2:&nbsp;p",<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"A1:&nbsp;q"<br />&nbsp;&nbsp;&nbsp;],<br />&nbsp;&nbsp;&nbsp;"defeat":[<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"A1>A3",<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"A1>A2"<br&nbsp;/><br />&nbsp;&nbsp;&nbsp;]<br />}
			</span>

            Alternatively, posting JSON:

			<span class="example" style="width:1000px;">
				curl -d<br /> 
				>'{"premises":["p","q"],"kbPrefs":["p&lt;q"],"rules":["[r1] p=&gt;s","[r2] q=&gt;t"],"rulePrefs":["[r1]&lt;[r2]"],"contrariness":["s-t"],"link":"weakest"}'<br />
				> <?php echo $base_url; ?>/evaluate"
			</span>
			
			Returns:
			
			<span class="example">
				<!--{"link":"weakest","kbPrefs":["p < q"],"semantics":"grounded","assumptions":[],"rulePrefs":["[r1] < [r2]"],"defeat":["A3>A4"],"axioms":[],"wellformed":true,"extensions":{"0":["A1","A2","A3"]},"premises":["p","q"],"acceptableConclusions":{"0":["p","q","t"]},"arguments":["A1: p","A2: q","A4: A1=>s","A3: A2=>t"],"contrariness":["s-t"]}-->
				{<br />&nbsp;&nbsp;&nbsp;"link":"weakest",<br />&nbsp;&nbsp;&nbsp;"kbPrefs":[<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"p&nbsp;<&nbsp;q"<br />&nbsp;&nbsp;&nbsp;],<br />&nbsp;&nbsp;&nbsp;"semantics":"grounded",<br />&nbsp;&nbsp;&nbsp;"assumptions":[<br /><br />&nbsp;&nbsp;&nbsp;],<br />&nbsp;&nbsp;&nbsp;"rulePrefs":[<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"[r1]&nbsp;<&nbsp;[r2]"<br />&nbsp;&nbsp;&nbsp;],<br />&nbsp;&nbsp;&nbsp;"defeat":[<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"A3>A4"<br />&nbsp;&nbsp;&nbsp;],<br />&nbsp;&nbsp;&nbsp;"axioms":[<br /><br />&nbsp;&nbsp;&nbsp;],<br />&nbsp;&nbsp;&nbsp;"wellformed":true,<br />&nbsp;&nbsp;&nbsp;"extensions":{<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"0":[<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"A1",<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"A2",<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"A3"<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]<br />&nbsp;&nbsp;&nbsp;},<br />&nbsp;&nbsp;&nbsp;"premises":[<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"p",<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"q"<br />&nbsp;&nbsp;&nbsp;],<br />&nbsp;&nbsp;&nbsp;"acceptableConclusions":{<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"0":[<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"p",<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"q",<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"t"<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]<br />&nbsp;&nbsp;&nbsp;},<br />&nbsp;&nbsp;&nbsp;"arguments":[<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"A1:&nbsp;p",<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"A2:&nbsp;q",<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"A4:&nbsp;A1=>s",<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"A3:&nbsp;A2=>t"<br />&nbsp;&nbsp;&nbsp;],<br />&nbsp;&nbsp;&nbsp;"contrariness":[<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"s-t"<br />&nbsp;&nbsp;&nbsp;]<br />}
			</span>
			<br />
			<h2>GET aifdb/&lt;argument&gt;</h2>

			<p>Description: evaluates the provided argument ID in <a href="/AIFdb">AIFdb</a>, returning a JSON description.</p>


