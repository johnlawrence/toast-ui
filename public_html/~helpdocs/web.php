<div class="info"><h1>TOAST user guide</h1>

<p>TOAST (The Online Argument Structures Tool) is a web-based implementation of the ASPIC+ framework, characterised by Prakken [2010].</p>

<p>There are two interfaces to TOAST - a web form and a web service. The web form allows you to enter an argumentation system, view the results of evaluating it. The web service interface allows you to connect your own applications to TOAST and recieve the results in JSON format. Documentation for the web service is <a href="api">available</a>.</p>

<p>The syntax for using the TOAST web interface is as follows:</p>

<h2>Knowledge base</h2>

<p>
    The knowledge base is split into Axioms, Premises and Assumptions. For all three sets, formulae are entered as a semi-colon seperated list of terms, with "~" being used for classical negation:
    <span class="example">
        p;<br />q;<br />something_in_prose;<br />~r;
    </span>

    TOAST also supports Prolog-style first-order predicates:
    <span class="example">
        software(toast);<br />
        drinks(bob, cola);
    </span>
    Note that the content of predicates must start with lowercase letters, and no formulae can contain spaces.
</p>

<h2>Knowledge base preferences</h2>

<p>
    The knowledge base prefereces are entered as a semi-colon seperated list of elements, with each element having the following format:

    <span class="example">
        lessPreferredFormula &lt; morePreferredFormula;
    </span> 

    Note that as per the ASPIC+ theory, axioms cannot be part of a preference relation.
</p>

<p>If the knowledge base is expressed in fist-order logic, preferences can include variables that quantify over all predicates of the given name:
    <span class="example">
        stayAtHome(X) < travel(X);
    </span>
    Note that variables must start with an uppercase letter.
</p>

<h2>Rules</h2>

<p>
    Rules are entered in the following format:
    <span class="example">
        [label] {comma-seperated list of antecedents}{implication}{consequent};
    </span>
    Implication is either "=>" (for a defeasible rule) or "->" for a strict rule.
</p>

<p>
    When using first-order logic, rules can be expressed as general patterns of inference using variables:
    <span class="example">
        [r1] software(X), useful(X) => use(X);<br />
        [r2] drinks(X, cola) => buy(X, cola);
    </span>
    All rules must be labelled. Rule labels are used for expressing undercutters:
    <span class="example">
        [r3] p => ~[r1];
    </span>
</p>    

<h2>Rule preferences</h2>

<p>
    Rule preferences are entered as a semi-colon seperated list of elements, with each element having the following format:
    <span class="example">
        [label_of_less_preferred_rule] &lt; [label_of_more_preferred_rule]
    </span>
    For example:
    <span class="example">
        [r1] &lt; [r2]
    </span>
    Note that as per the ASPIC+ theory, strict rules cannot be part of a  preference relation.
</p>

<h2>Contrariness</h2>

<p>
    Contrariness relations allow one formula to be declared the contrary of another, or for two formulae to be declared contradictory.
</p>

<p>
    TOAST accepts both these these forms. Examples will be used to describe the syntax:
    <span class="example"> 
        p^q;
    </span>
    This means that p is a contrary of q.
    <span class="example">
        r-s;
    </span>
    This means that r and s are contradictory, and is the same as:
    <span class="example">
        r^s;<br />
        s^r;
   </span>
   
   As with preferences, general contrariness can be defined when using first-order logic:
   <span class="example">
        food(X)-drink(X);
   </span>
    This means that all things declared as food contradict all things declared as drink, and vice versa.
</p>

<h2>Query</h2>

<p>The query determines whether or not there is an acceptable argument for the
given formula, under the specified semantics, in the abstract framework derived from the given AS.</p>

<h2>Last link/weakest link</h2>

<p>This decides whether the last link or weakest link principle is used to
determine the ordering on arguments. Please refer to [Prakken 2010] for details.</p>

<h2>References</h2>

<p>H. Prakken. An abstract framework for argumentation using structured
arguments. <i>Argument and Computation</i>, 1(2):93-124, 2010.</p>

</div>
