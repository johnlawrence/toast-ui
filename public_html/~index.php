			<div class="container">
				<form method="POST" action="/toast/include/toast.ajax.php" id="aspicForm">		
					<input type="hidden" id="aifdb" name="aifdb" value="<?php echo (isset($_GET['nodeset'])) ? "true" : "false"; ?>" />
					<div id="axioms" class="formInput" style="float:left; padding:5px;">
						Axioms:<br /><textarea name="axioms" rows="10" cols="30"><?php echo stripslashes($axioms); ?></textarea>
						</div>
					<div id="premises" class="formInput" style="float:left; padding:5px;">
						Premises:<br /><textarea id="txtPremises" name="premises" rows="10" cols="30"><?php echo stripslashes($premises); ?></textarea>
					</div>
					<div id="assumptions" class="formInput" style="float:left; padding:5px;">
						Assumptions:<br /><textarea id="txtAssumptions" name="assumptions" rows="10" cols="30"><?php echo stripslashes($assumptions); ?></textarea>
					</div>
					<div id="kbPrefs" class="formInput" style="float:left; padding:5px;">
						Preferences:<br /><textarea id="txtKbPrefs" name="kbprefs" rows="10" cols="30"><?php echo stripslashes($kbPrefs); ?></textarea>
					</div>
					<br />		
					<div id="rules" class="formInput" style="float:left; padding:5px;">
						Rules:<br /><textarea id="txtRules" name="rules" rows="10" cols="30"><?php echo stripslashes($rules); ?></textarea>
					</div>
					<div id="rulePrefs" class="formInput" style="float:left; padding-left:5px; padding-top:5px; padding-bottom:5px;">
						Rule Preferences:<br /><textarea id="txtRulePrefs" name="ruleprefs" rows="10" cols="30"><?php echo stripslashes($rulePrefs); ?></textarea>
					</div>
					<div id="contrariness" class="formInput" style="float:left; padding-left:0px; padding-top:5px; padding-bottom:5px;">
						Contrariness:<br /><textarea id="txtContrariness" name="contrariness" rows="10" cols="30"><?php echo $contrariness; ?></textarea>
					</div>
					<div id="oth" class="formInput" style="float:left; padding-left:5px; padding-top:5px; padding-bottom:5px;">
						Preference principle: <br /><br />
						<input type="radio" id="chkLast" name="link" value="last" <?php if($link=="last" || !isset($link)) echo "checked"; ?> />Last link <br/>
						<input type="radio" id="chkWeakest" name="link" value="weakest" <?php if($link=="weakest") echo "checked"; ?>/>Weakest link<br /><br />Semantics:<br /><br />
						<select name="semantics">
							<option id="optGrounded" value="GROUNDED" <?php if($semantics=="GROUNDED" || !isset($semantics)) echo "SELECTED"; ?>>Grounded</option>
							<option id="optPreferred" value="PREFERRED" <?php if($semantics=="PREFERRED") echo "SELECTED"; ?>>Preferred</option>
							<option id="optSemiStable" value="SEMISTABLE" <?php if($semantics=="SEMISTABLE") echo "SELECTED"; ?>>Semi-Stable</option>
							<option id="optStable" value="STABLE" <?php if($semantics=="STABLE") echo "SELECTED"; ?>>Stable</option>
						</select>
					</div>
					<div id="query" class="formInput" style="float:left; padding-left:5px; padding-top:5px; padding-bottom:5px;">
						<div style="vertical-align:middle;">Query:
							<input id="txtQuery" type="text" name="query" value="<? echo stripslashes($query); ?>" />
							<input type="submit" value="Evaluate" id="process" />
							<a href="javascript:loadExample();">Load an example</a> | 
							<a href="/toast/help/web">Help</a> | 
							<a href="/toast/help/api">API</a>
						</div>
					</div>
				</form>
			</div>

